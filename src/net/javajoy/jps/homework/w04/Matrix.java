package net.javajoy.jps.homework.w04;

/**
 * Class Matrix implements InterfaceMatrix interface.
 * <p/>
 *
 * @author Sergey Mikhluk
 */
public class Matrix implements IMatrix {
    private int cell[][];   // матрица произвольной размерности, матрица конкретной размерности инициализируется в конструктору
    // куда передается либо размерность, либо матрица.
    // доступ к cell через геттер

    /**
     * Constructor with one param.
     * Create empty object with given dimension
     *
     * @param matrixDimension number that set dimension of matrix
     */
    public Matrix(int matrixDimension) {
        this.cell = new int[matrixDimension][matrixDimension];
    }

    /**
     * Constructor with one param.
     * Create object with given array
     *
     * @param cell matrix of array type
     */
    public Matrix(int[][] cell) { //по правилам хорошего API, нужно еще реализовать валидацию массива на предмет правильное матрицы. Вдруг кто-то передаст не квадраную матрицу?
        int matrixDimension = cell.length;
        this.cell = new int[matrixDimension][matrixDimension];

        //копируем матрицу поячеечно, иначе просто копируется ссылка и потом в результае вычислений изменяется исходный массив arrayA инициализируемый в классе main
        for (int row = 0; row < matrixDimension; row++) {
            System.arraycopy(cell[row], 0, this.cell[row], 0, matrixDimension);
        }
    }

    public int[][] getCell() {
        return cell;
    }

    /**
     * Method adding two matrix.
     * Method change this matrix
     *
     * @param iMatrixB object of Matrix type
     */
    @Override
    public IMatrix add(IMatrix iMatrixB) { //по правилам хорошего API, нужно еще реализовать валидацию второго операнда: размерность матрицы

        Matrix matrixB = (Matrix) iMatrixB;
        int matrixDimension = this.cell.length;

        for (int row = 0; row < matrixDimension; row++) {
            for (int col = 0; col < matrixDimension; col++) {
                this.cell[row][col] = this.cell[row][col] + matrixB.cell[row][col];
            }
        }
        return this;
    }

    /**
     * Method multiply two matrix.
     * Method change this matrix
     *
     * @param iMatrixB object of Matrix type
     */
    @Override
    public IMatrix multiply(IMatrix iMatrixB) { //по правилам хорошего API, нужно еще реализовать валидацию второго операнда: размерность матрицы

        Matrix matrixB = (Matrix) iMatrixB;//приводим объект типа интерфейс к объекту типа матрица
        int matrixDimension = this.cell.length;

        //создаем копию матрицы А, т.к. результат умножения будет записываться сразу в матрицу А
        int[][] cellA = new int[matrixDimension][matrixDimension];
        for (int row = 0; row < matrixDimension; row++) {
            System.arraycopy(this.cell[row], 0, cellA[row], 0, matrixDimension);
        }

        for (int row = 0; row < matrixDimension; row++) {
            for (int col = 0; col < matrixDimension; col++) {
                this.cell[row][col] = 0;
                for (int counter = 0; counter < matrixDimension; counter++) {
                    this.cell[row][col] = this.cell[row][col] + cellA[row][counter] * matrixB.cell[counter][col];
                }
            }
        }
        return this;
    }

    /**
     * Method converts current matrix to the string.
     *
     * @return string for print to the console
     */
    @Override
    public String toString() {
        String str = "";
        for (int[] aCell : cell) {
            for (int col = 0; col < cell[0].length; col++) {
                str = str + aCell[col] + " ";
            }
            str = str + "\n";
        }
        return str;
    }
}
