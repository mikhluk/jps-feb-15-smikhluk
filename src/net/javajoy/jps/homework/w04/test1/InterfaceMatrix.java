package net.javajoy.jps.homework.w04.test1;

// Также можно настроить автогенерирование комментариев.

/**
 * Interface InterfaceMatrix Provides methods for working with matrix
 *
 * @author Sergey Mikhluk
 */

public interface InterfaceMatrix {
    /**
     * Method multiply two matrix.
     * Method change this matrix
     *
     * @param matrixA object of Matrix type
     * @param matrixB object of Matrix type
     */
    void multiply(InterfaceMatrix matrixA, InterfaceMatrix matrixB);

    /**
     * Method adding two matrix.
     * Method change this matrix
     *
     * @param matrixA object of Matrix type
     * @param matrixB object of Matrix type
     */
    void add(InterfaceMatrix matrixA, InterfaceMatrix matrixB);

    // TODO: AS: подумай как можно повысить абстракцию этих методов. Я про параметры.
}
