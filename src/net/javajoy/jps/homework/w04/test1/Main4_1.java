package net.javajoy.jps.homework.w04.test1;

/**
 * Home task 04
 * 1. Создайте интерфейс «Матрица» с операциями сложения и умножения.
 * Реализуйте этот интерфейс:
 * в обычном классе
 * в  неизменном (immutable) классе
 *
 * Примечание: Второй класс сделайте наследником первого (а можно ли сделать наоборот?).
 * <p/>
 * 2. Задокументируйте классы и методы
 *
 * @author Sergey Mikhluk
 */
public class Main4_1 {
    public static void main(String[] args) {

        // Создаем и выводим на печать Матрицу 1
        int arrayA[][] = {
                {1, 3},
                {5, 7}};
        Matrix matrixA = new Matrix(arrayA);
        System.out.println("Матрица A:\n" + matrixA);

        // Создаем и выводим на печать Матрицу 2
        int arrayB[][] = {
                {2, 4},
                {6, 8}};
        Matrix matrixB = new Matrix(arrayB);
        System.out.println("Матрица B:\n" + matrixB);


        int matrixDimension = arrayA.length;  //определяем разменость матрицы
        Matrix matrixC = new Matrix(matrixDimension);
        matrixC.add(matrixA, matrixB);
        System.out.println("Результат сложения матриц:\n" + matrixC);

        Matrix matrixD = new Matrix(matrixDimension);
        matrixD.multiply(matrixA, matrixB);
        System.out.println("Результат умножения матриц:\n" + matrixD);
         // можно проверить тут - http://matrixcalc.org/#%7B%7B1,3%7D,%7B5,7%7D%7D%2A%7B%7B2,4%7D,%7B6,8%7D%7D

    }

}
