package net.javajoy.jps.homework.w04.test1;

/**
 * Class Matrix implements InterfaceMatrix interface.
 * <p/>
 *
 * @author Sergey Mikhluk
 */
public class Matrix implements InterfaceMatrix {
    private int cell[][];   // матрица произвольной размерности, матрица конкретной размерности инициализируется в конструктору
    // куда передается либо размерность, либо матрица.


    /**
     * Constructor with one param.
     * Create empty object with given dimension
     *
     * @param matrixDimension number that set dimension of matrix
     */
    public Matrix(int matrixDimension) {
        this.cell = new int[matrixDimension][matrixDimension];
    }

    /**
     * Constructor with one param.
     * Create object with given array
     *
     * @param cell matrix of array type
     */
    public Matrix(int[][] cell) {
        this.cell = new int[cell.length][cell[0].length]; // MS: правильно инициализировано?
        this.cell = cell; // MS: записывает матрицу из парамерта setCell, или лучше через каждую ячейку?
    }

    /**
     * Method multiply two matrix.
     * Method change this matrix
     *
     * @param iMatrixA object of Matrix type
     * @param iMatrixB object of Matrix type
     */
    @Override
    public void multiply(InterfaceMatrix iMatrixA, InterfaceMatrix iMatrixB) {

        Matrix matrixA = (Matrix) iMatrixA; //приводим объект типа интерфейс к объекту типа матрица
        Matrix matrixB = (Matrix) iMatrixB;

        for (int row = 0; row < this.cell.length; row++) {
            for (int col = 0; col < this.cell[0].length; col++) {
                this.cell[row][col] = 0;
                for (int counter = 0; counter < this.cell.length; counter++) {   //вообще-то должна быть длина матриц А и Б а не матрицы С, но для квадратных матриц размерность совпадает
                    this.cell[row][col] = this.cell[row][col] + matrixA.cell[row][counter] * matrixB.cell[counter][col];
                    //MS: почему результирующая матрица получается транспонированной пока непомнятно(
                }
            }
        }
    }

    /**
     * Method adding two matrix.
     * Method change this matrix
     *
     * @param iMatrixA object of Matrix type
     * @param iMatrixB object of Matrix type
     */
    @Override
    public void add(InterfaceMatrix iMatrixA, InterfaceMatrix iMatrixB) {

        Matrix matrixA = (Matrix) iMatrixA; //приводим объект типа интерфейс к объекту типа матрица
        Matrix matrixB = (Matrix) iMatrixB;

        for (int row = 0; row < cell.length; row++) {
            for (int col = 0; col < cell[0].length; col++) {
                cell[row][col] = matrixA.cell[row][col] + matrixB.cell[row][col];
            }
        }
    }

    /**
     * Method converts current matrix to the string.
     *
     * @return string for print to the console
     */
    @Override
    public String toString() {
        String str = "";
        for (int row = 0; row < cell.length; row++) {
            for (int col = 0; col < cell[0].length; col++) {
                str = str + cell[col][row] + " ";
            }
            str = str + "\n";
        }
        return str;
    }
}
