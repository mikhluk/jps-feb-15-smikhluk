package net.javajoy.jps.homework.w04;

/**
 * Home task 04
 * 1. Создайте интерфейс «Матрица» с операциями сложения и умножения.
 * Реализуйте этот интерфейс:
 * в обычном классе
 * в  неизменном (immutable) классе
 * <p/>
 * Примечание: Второй класс сделайте наследником первого (а можно ли сделать наоборот?).
 * <p/>
 * 2. Задокументируйте классы и методы
 *
 * @author Sergey Mikhluk
 */
public class Main04 {
    public static void main(String[] args) {

        // Создаем и выводим на печать Матрицу A
        int[][] arrayA = {
                {1, 3},
                {5, 7}};

        IMatrix matrixA = new Matrix(arrayA);
        System.out.println("Матрица A:\n" + matrixA);

        // Создаем и выводим на печать Матрицу B
        int arrayB[][] = {
                {2, 4},
                {6, 8}};
        IMatrix matrixB = new Matrix(arrayB);
        System.out.println("Матрица B:\n" + matrixB);

        IMatrix matrixC = new Matrix(arrayA); //копирум в Матрицу С массив А
        matrixC.add(matrixB);
        System.out.println("Результат сложения матриц:\n" + matrixC);

        IMatrix matrixD = new Matrix(arrayA);
        matrixD.multiply(matrixB);
        System.out.println("Результат умножения матриц:\n" + matrixD);

        //реализация сложения и умножения через Immutable matrix
        IMatrix imMatrixA = new ImmutableMatrix(arrayA);
        IMatrix imMatrixB = new ImmutableMatrix(arrayB);

        System.out.println("Результат сложения матриц в immutable классе:\n" + imMatrixA.add(imMatrixB));
        System.out.println("Результат умножения матриц в immutable классе:\n" + imMatrixA.multiply(imMatrixB));
    }

}
