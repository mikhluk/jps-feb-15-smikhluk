package net.javajoy.jps.homework.w04.test;

/**
 * Created by Serg on 22.03.2015.
 */
class Parent {
    Parent(){
        System.out.println(this);
    }
    public String toString() {
        return "Parent";
    }
}
