package net.javajoy.jps.homework.w04.test;

/**
 * Created by Serg on 22.03.2015.
 */
public class Main {
    static void f1(Parent a){
        System.out.println("Parent: " + a);
    }
    static void f1(Child a){
        System.out.println("Child: " + a);
    }

    public static void main(String[] args) {
        Parent a = new Child();
        f1(a);
        f1((Child)a);
    }

}
