package net.javajoy.jps.homework.w04;

/**
 * Immutable class, which extends class Matrix
 *
 * @author smikhluk
 */
public final class ImmutableMatrix extends Matrix {

    /**
     * Constructor with one param.
     * Create empty object with given dimension
     *
     * @param matrixDimension number that set dimension of matrix
     */
    public ImmutableMatrix(int matrixDimension) {
        super(matrixDimension);
    }


    /**
     * Constructor with one param.
     * Create object with given array
     *
     * @param cell matrix of array type
     */
    public ImmutableMatrix(int[][] cell) {
        super(cell);
    }

    /**
     * Method adding two matrix.
     * Method change this matrix
     *
     * @param iMatrixB object of Matrix type
     */
    @Override
    public IMatrix add(IMatrix iMatrixB) {

        int matrixDimension = this.getCell().length;
        int[][] cellSum = new int[matrixDimension][matrixDimension];

        Matrix matrixB = (Matrix) iMatrixB;

        // сохраняем в cellSum результат сложения матриц
        for (int row = 0; row < matrixDimension; row++) {
            for (int col = 0; col < matrixDimension; col++) {
                cellSum[row][col] = this.getCell()[row][col] + matrixB.getCell()[row][col];
            }
        }

        return new ImmutableMatrix(cellSum);
    }

    /**
     * Method multiply two matrix.
     * Method change this matrix
     *
     * @param iMatrixB object of Matrix type
     */
    @Override
    public IMatrix multiply(IMatrix iMatrixB) {
        int matrixDimension = this.getCell().length;
        int[][] cellMult = new int[matrixDimension][matrixDimension];

        Matrix matrixB = (Matrix) iMatrixB;//приводим объект типа интерфейс к объекту типа матрица

        //создаем копию матрицы А
        for (int row = 0; row < matrixDimension; row++) {
            System.arraycopy(this.getCell()[row], 0, cellMult[row], 0, matrixDimension);
        }

        for (int row = 0; row < matrixDimension; row++) {
            for (int col = 0; col < matrixDimension; col++) {
                cellMult[row][col] = 0;
                for (int counter = 0; counter < matrixDimension; counter++) {
                    cellMult[row][col] = cellMult[row][col] + this.getCell()[row][counter] * matrixB.getCell()[counter][col];
                }
            }
        }

        return new ImmutableMatrix(cellMult);
    }
}
