package net.javajoy.jps.homework.w04;

// Также можно настроить автогенерирование комментариев.

/**
 * Interface InterfaceMatrix Provides methods for working with matrix
 *
 * @author Sergey Mikhluk
 */

public interface IMatrix {
    /**
     * Method multiply two matrix.
     * Method change this matrix
     *
     * @param matrixB object of Matrix type
     */

    IMatrix multiply(IMatrix matrixB);  // в параметрах максимальный уровень абстракции.


    /**
     * Method adding two matrix.
     * Method change this matrix
     *
     * @param matrixB object of Matrix type
     */
    IMatrix add(IMatrix matrixB); // в параметрах максимальный уровень абстракции

}
