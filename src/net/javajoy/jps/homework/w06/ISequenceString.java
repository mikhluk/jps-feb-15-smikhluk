package net.javajoy.jps.homework.w06;

/**
 * Интерфейс последовательность строк
 *
 * @author Sergey Mikhluk
 */
public interface ISequenceString extends Iterable<String> {
    String searchByIndex(int index);
    int searchByValue(String str);
    void add(String str);
    void add(String str, int index);
    void delete(int index);
}
