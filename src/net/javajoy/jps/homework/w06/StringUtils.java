package net.javajoy.jps.homework.w06;

/**
 * Created by Serg on 17.05.2015.
 */
public class StringUtils {
    public static final String KEY = "{}()";

    public static boolean isBracket(char c) {
        return KEY.contains(String.valueOf(c));
    }

    public static boolean isPairBracket(String element, String stackElement) {

        if (stackElement != null) {
            switch (element) {
                case "{":
                    return stackElement.equals("}");
                case "(":
                    return stackElement.equals(")");
                case ")":
                    return stackElement.equals("(");
                case "}":
                    return stackElement.equals("{");
            }
        }

        return false;
    }
}