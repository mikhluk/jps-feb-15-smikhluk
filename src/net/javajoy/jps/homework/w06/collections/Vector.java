package net.javajoy.jps.homework.w06.collections;

import net.javajoy.jps.homework.w06.ISequenceString;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;


/**
 * Класс Vector реализовывающий Вектор Строк.
 *
 * @author Sergey Mikhluk
 */
public class Vector implements ISequenceString, Iterable<String>  {
    private static final int DEFAULT_CAPACITY = 5;

    private String[] data;
    private int capacity;
    private int size;
    private int cursor;

    public Vector(int capacity) {
        this.capacity = capacity;
        data = new String[this.capacity];
    }

    public Vector() {
        this(DEFAULT_CAPACITY);
    }


    @Override
    public Iterator<String> iterator() {
        return new vectorIterator();
    }

    /**
     * Внутренний класс ListIterator
     */
    private class vectorIterator implements Iterator<String> {

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public String next() {
            String value = data[cursor];
            cursor++;
            return value;
        }

        @Override
        public void remove() {
            //MS: что тут должно быть???
        }
    }


    @Override
    public String searchByIndex(int index) {
        return data[index];
    }

    @Override
    public int searchByValue(String str) {
        int foundIndex = -1;
        for (int i = 0; i < size; i++) {
            if (Objects.equals(data[i], str)) { //AN: строки сравниваем через equals
                foundIndex = i;
                break;
            }
        }
        return foundIndex;
    }


    //getters
    public String getValue(int index) {
        return data[index];
    }

    public int getCapacity() {
        return capacity;
    }

    public int getSize() {
        return size;
    }

    @Override
    public void add(String str) {
        if (size < capacity) {
            data[size] = str;
            size++;
        } else {
            String[] newData = new String[capacity];

            System.arraycopy(data, 0, newData, 0, size);

            capacity = capacity + (capacity / 2);
            data = new String[capacity];
            System.arraycopy(newData, 0, data, 0, size);
            data[size] = str;
            size++;
        }
    }

    @Override
    public void add(String str, int index) {
        String[] newData = new String[capacity];
        System.arraycopy(data, 0, newData, 0, size);

        if (size >= capacity) {
            capacity = capacity + (capacity / 2);
            data = new String[capacity];
            System.arraycopy(newData, 0, data, 0, size);
        }


        data[index] = str;
        System.arraycopy(newData, index, data, index + 1, size - index);
        size++;

    }

    @Override
    public void delete(int index) {

        String[] newData = new String[capacity];
        System.arraycopy(data, 0, newData, 0, size);

        if (index < 0 || index >= size) {
            System.out.println("Неверный индекс!");
            return;
        }

        // если удаляется не последний элемент вектора,
        // если index = size -1 то это значит, что мы удаляем последний элемент вектора, сдвигать ничего не нужно
        if (index < size - 1) {
            // сдвигаем все элементы вектора начиная со следующей позиции, на одну позицию назад
            System.arraycopy(newData, index + 1, data, index, size - index - 1);
        }

        // об-null-яем последний элемент вектора
        data[size - 1] = null;
        size--;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (String s : data) {
            if (s != null) {
                if (!builder.toString().isEmpty()) builder.append(", ");
                builder.append(s); //AN: если в списке элемент со значением null, то он нам не нужен в String. Это же запасной элемент как часть общего capacity
            }
        }

        return String.format("Vector{data=%s, capacity=%d, size=%d}", builder.toString(), capacity, size);
    }
}
