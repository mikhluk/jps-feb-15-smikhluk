package net.javajoy.jps.homework.w06.collections;

/**
 * Created by Serg on 05.05.2015.
 */
public class Stack {
    private Vector stack;

    public Stack() {
        stack = new Vector();
    }

    public void push(String value){
        stack.add(value);
    }

    public String pop() {
        String value = stack.searchByIndex(stack.getSize() - 1);  // запоминаем значение последнего элемента
        stack.delete(stack.getSize()-1);  // удаляем последний элемент
        return value;
    }

    public String peek() {
        return (stack.getSize()>0) ?  stack.searchByIndex(stack.getSize() - 1): null;
    }

    public int getSize() {
        return stack.getSize();
    }

    public String toString() { //AN: удалил старый код
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <= getSize(); i++) {
            if (stack.searchByIndex(i) != null) {
                builder.append(stack.searchByIndex(i)).append(", ");
            }
        }

        return builder.toString();
    }

}
