package net.javajoy.jps.homework.w06.collections;

import net.javajoy.jps.homework.w06.ISequenceString;

import java.util.Iterator;

/**
 * Класс List реализовывающий Список Строк.
 *
 * @author Sergey Mikhluk
 */
public class List implements ISequenceString { //AN: я вытянул Iterable в родители к интерфейсу ISequenceString. В итоге это нам позволило использовать цикл foreach в тесте без приведения к типу List или Vector
    public Node head;

    @Override
    public Iterator<String> iterator() {
        return new ListIterator();
    }

    /**
     * Внутренний класс ListIterator
     */
    private class ListIterator implements Iterator<String> {
        Node node = head;
        Node prevNode;

        @Override
        public boolean hasNext() {
            return node != null;
        }

        @Override
        public String next() {
            String value = node.value;
            node = node.next;
            return value;
        }

        @Override
        public void remove() {
            prevNode.next = node.next;
        }
    }

    /**
     * Внутренний класс Node
     */
    public static class Node {
        private String value = "";
        public Node next = null;

        Node(String value, Node next) {
            this.value = value;
            this.next = next;
        }

        public String getValue() {
            return value;
        }
    }

    public void clear() {
        head = null;
    }

    @Override
    public String searchByIndex(int index) {
        Node node;
        int i = 0;

        for (node = head; node != null && i != index; node = node.next) {
            i++;
        }

        return node == null ? "-1" : node.value;
    }

    @Override
    public int searchByValue(String value) {

        Node node;
        int i = 0;

        for (node = head; node != null && !node.value.equals(value); node = node.next) {
            i++;
        }

        return node == null ? -1 : i;
    }

    @Override
    public void add(String value) {
        if (head == null) {
            head = new Node(value, null);
            return;
        }

        Node node = head;

        while (node.next != null) {
            node = node.next;
        }

        node.next = new Node(value, null);
    }

    @Override
    public void add(String value, int index) {
        Node node;
        int i = 0;

        if (index == 0) {
            Node tempNode = head;
            head = new Node(value, tempNode);
            return;
        }

        for (node = head; node != null && i != index - 1; node = node.next) {
            i++;
        }

        if (node == null) {
            return;
        }

        Node tempNode = node.next;
        node.next = new Node(value, tempNode);
    }

    @Override
    public void delete(int index) {
        Node node;
        int i = 0;
        if (index == 0) {
            head = head.next;
            return;
        }

        for (node = head; node != null && i != index - 1; node = node.next) {
            i++;
        }

        if (node == null) {
            return;
        }

        Node tempNode = node.next;
        node.next = tempNode.next;

    }

    @Override
    public String toString() {
        String str = "List{data =[";
        int i = 0;
        Node node;

        for (node = head; node != null; node = node.next) {
            if (i > 0) {
                str = str + ", ";
            }

            if (null != node.value) str = str + node.value; //AN: если в спиксе элемент со значением null, то он нам не нужен в String. Это же запасной элемент как часть общего capacity
            i++;
        }
        str = str + "], size = " + i + "}";

        return str;
    }

}
