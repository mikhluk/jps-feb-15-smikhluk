package net.javajoy.jps.homework.w06;

import net.javajoy.jps.homework.w06.collections.List;
import net.javajoy.jps.homework.w06.collections.Stack;
import net.javajoy.jps.homework.w06.collections.Vector;

import java.util.Iterator;

/**
 * Home task 06
 * See readme.md
 *
 * @author Sergey Mikhluk
 */
public class Main06 {

    public static void testSequenceString(ISequenceString sequenceString) {
        final int searchId = 4; //AN: так как это переменные, то они должны быть с маленькой буквы и без подчеркиваний
        final int addId = 0;
        final int deleteId = 4;
        String searchValue = "пять";

        // простое добавление значения в конец вектора
        System.out.println("1. Создаем последовательность строк");
        sequenceString.add("раз ");
        sequenceString.add("два");
        sequenceString.add("три");
        System.out.println(sequenceString);

        // добавление в конец с увеличением емкости вектора
        System.out.println("\n2. Добавляем значения в конец с увеличением емкости");
        sequenceString.add("четыре");
        sequenceString.add("пять");
        sequenceString.add("шесть");
        sequenceString.add("семь");
        System.out.println(sequenceString);

        // добавление значения на указанную позицию
        System.out.println("\n3. Добавляем значение на указанную позицию " + addId);
        sequenceString.add("четыре с половиной", addId);
        System.out.println(sequenceString);

        // удаление элемента последовательности с указанной позиции
        System.out.println("\n4. Удаляем значение с позиции " + deleteId);
        sequenceString.delete(deleteId);
        System.out.println(sequenceString);

        System.out.printf("\nНайдем элемент по индексу:  %d - значение: \"%s\" \n", searchId, sequenceString.searchByIndex(searchId));
        System.out.printf("Найдем элемент по значению: \"%s\" - индекс: %d \n", searchValue, sequenceString.searchByValue(searchValue));

        //if (sequenceString instanceof Vector) {
        System.out.println("\n5.Пример печати Вектора строк через цикл for each");
        for (String element : sequenceString) {
            System.out.print(element + ", ");
        }

        System.out.println("\n\n6.Пример печати через Итератор");
        Iterator it = sequenceString.iterator(); //AN: убрал приведение типа, так как ISequenceString теперь потомок Iterable
        while (it.hasNext()) {
            System.out.print(it.next() + ", ");
        }

        //AN: проверка на instanceOf не нужна была. Два куска кода делали одинаковое
//        }

//        if (sequenceString instanceof List) {
//            System.out.println("\n5.Пример печати Вектора строк через цикл for each");
//            for (String element : (List) sequenceString) {
//                System.out.print(element + ", ");
//            }
//
//            System.out.println("\n\n6.Пример печати через Итератор");
//            Iterator it = ((List)sequenceString).iterator();
//            while (it.hasNext()) {
//                System.out.print(it.next() + ", ");
//            }
//        }
    }

    public static void testStack() {
        Stack strStack = new Stack();

        strStack.push("раз");
        strStack.push("два");
        strStack.push("три");
        strStack.push("четыре");
        strStack.push("пять");
        System.out.println("1. Создали СТЕК");

        System.out.println("\n2. Достанем и распечатаем все элементы СТЕКА");
        String strValue;
        while (strStack.getSize() > 0) {
            strValue = strStack.pop();
            System.out.print(strValue + ", ");
        }

    }

    public static void testBracketsWithStack() throws Exception {
        Stack strStack = new Stack();
        String testString = "(a+b = {c})";

        System.out.println("Строка:" + testString);

        for (char c : testString.toCharArray()) {
            if (StringUtils.isBracket(c)) {
                if (StringUtils.isPairBracket(String.valueOf(c), strStack.peek())) {
                    strStack.pop();
                } else {
                    strStack.push(String.valueOf(c));
                }
            }
        }

        System.out.printf("Stack content is %s, result: %s%n",
                strStack, strStack.getSize() == 0 ? "OK" : "WRONG");

    }

    public static void main(String[] args) throws Exception {
        //AN: уже намного лучше.
        System.out.println("================================== ВЕКТОР =================================");
        testSequenceString(new Vector());  // запускаем реализацию последовательности строк на основе Вектора
        //AN: ссылка не нужна, сразу отдаем туда объект


        System.out.println("\n\n================================== СПИСОК =================================");
        testSequenceString(new List());    // запускаем реализацию последовательности строк на основе Списка

        System.out.println("\n\n================================== СТЕК =================================");
        testStack();

        System.out.println("\n\n================================== Проверка расстановки скобок с помощью СТЕК =================================");
        testBracketsWithStack();
    }

}
