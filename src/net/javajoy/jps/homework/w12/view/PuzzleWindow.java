package net.javajoy.jps.homework.w12.view;

import net.javajoy.jps.homework.w12.controller.KeyActionListener;
import net.javajoy.jps.homework.w12.controller.MenuActionListener;
import net.javajoy.jps.homework.w12.controller.MouseActionListener;
import net.javajoy.jps.homework.w12.model.PuzzleBoard;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class PuzzleWindow extends JFrame {

    public static final String MAIN_WINDOW_TITLE = "Puzzle window";
    public static final String RESOURCE_PATH = "resources/w12";
    public static final int MAIN_WINDOW_WIDTH = 680;
    public static final int MAIN_WINDOW_HEIGHT = 580;
    public static final int LAST_CELL = 15;
    public static final String MENU_RANDOM = "Random";
    public static final String MENU_GATHERED = "Gathered";
    public static final String MENU_EXIT = "Exit";

    private PuzzleBoard puzzleBoard;

    private JPanel mainPanel;
    private JPanel statusPanel;
    private JPanel boardPanel;
    private JMenu menu;

    public PuzzleWindow(PuzzleBoard puzzleBoard) throws IOException {
        super(MAIN_WINDOW_TITLE);

        this.puzzleBoard = puzzleBoard;
        puzzleBoard.fillBoardAscending();

        createMenu();
        createStatusFrame();
        createBoardFrame();
        createMainFrame();

        refreshView();
        display();
        setResizable(false);
    }

    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();
        menu = new JMenu("Menu");

        JMenuItem menuRandom = new JMenuItem(MENU_RANDOM);
        menuRandom.setActionCommand(MENU_RANDOM);

        JMenuItem menuGathered = new JMenuItem(MENU_GATHERED);
        menuGathered.setActionCommand(MENU_GATHERED);
        JMenuItem menuExit = new JMenuItem(MENU_EXIT);
        menuExit.setActionCommand(MENU_EXIT);

        menu.add(menuRandom);
        menu.addSeparator();
        menu.add(menuGathered);
        menu.addSeparator();
        menu.add(menuExit);

        menuBar.add(menu);
        setJMenuBar(menuBar);
    }

    private void createStatusFrame() {
        statusPanel = new JPanel(new GridLayout(4, 1));
        statusPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        JLabel label1 = new JLabel("Play");
        JLabel label2 = new JLabel("Now!");

        label1.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        label1.setText("Play");
        label2.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        label2.setText("Now!");

        statusPanel.add(label1);
        statusPanel.add(label2);
    }

    private void createBoardFrame() {
        boardPanel = new JPanel(new GridLayout(puzzleBoard.getBoardSize(), puzzleBoard.getBoardSize()));
        boardPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
            for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
                final JButton boardButton = new JButton();
                boardButton.setFocusable(false);

                boardButton.setActionCommand(Integer.toString(getNumberOfComponent(y, x)));
                boardPanel.add(boardButton);
            }
        }
    }

    private void createMainFrame() {
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.red));

        mainPanel.add(statusPanel, new GridBagConstraints(
                        0, 0, 1, 2, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(5, 5, 5, 5), 0, 0)
        );

        mainPanel.add(boardPanel, new GridBagConstraints(
                        1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(5, 0, 5, 0), 0, 0)
        );

        mainPanel.setFocusable(true);
        getContentPane().add(mainPanel, BorderLayout.CENTER);

        setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public void refreshView() throws IOException {
        for (int i = 0; i < puzzleBoard.getBoardSize(); i++) {
            for (int j = 0; j < puzzleBoard.getBoardSize(); j++) {
                JButton boardButton = (JButton) boardPanel.getComponent(getNumberOfComponent(i, j));

                String strCellsValue = Integer.toString(puzzleBoard.getCellValue(i, j));

                if (strCellsValue.equals(Integer.toString(LAST_CELL))) {
                    strCellsValue = "";  // последнюю клетку рисуем пустую
                    boardButton.setBorder(BorderFactory.createLineBorder(Color.RED));
                } else {
                    boardButton.setBorder(BorderFactory.createLineBorder(Color.CYAN));
                }

                String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();
                //String imgRoot = getClass().getResource("/w12").toString().replace("file:/", "");
                boardButton.setIcon(new ImageIcon(String.format("%s/ku_%s.jpg", imgRoot, strCellsValue)));
            }
        }
    }

    public void display() {
        setVisible(true);
    }

    public int getNumberOfComponent(int y, int x) {
        return y * puzzleBoard.getBoardSize() + x;
    }

    public JPanel getBoardPanel() {
        return boardPanel;
    }

    public JMenu getMenu() {
        return menu;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }


    public void addMenuActionListener(MenuActionListener menuActionListener) {
        for (int i = 0; i < getMenu().getItemCount(); i++) {
            JMenuItem menuItem = getMenu().getItem(i);
            if (menuItem != null) {
                getMenu().getItem(i).addActionListener(menuActionListener);
            }
        }
    }

    public void addBoardKeyListener(KeyActionListener keyActionListener) {
        getMainPanel().addKeyListener(keyActionListener);
    }

    public void addGameButtonActionListener(MouseActionListener mouseActionListener) {

        for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
            for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
                JButton jButton = (JButton) getBoardPanel().getComponent(getNumberOfComponent(y, x));
                jButton.addActionListener(mouseActionListener);
            }
        }
    }
}





