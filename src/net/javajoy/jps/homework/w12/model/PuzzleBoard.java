package net.javajoy.jps.homework.w12.model;

import net.javajoy.jps.homework.w12.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class PuzzleBoard {

    public static final int FLAG_COL = 1;
    public static final int FLAG_ROW = 0;

    private int cells[][];

    public PuzzleBoard(int boardSize) {
        this.cells = new int[boardSize][boardSize];
    }

    public void fillBoardAscending() {
        int value = 0;
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells[0].length; x++) {
                cells[y][x] = value++;
            }
        }
    }

    public void fillBoardRandom() {
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells[0].length; x++) {
                cells[y][x] = (int) (Math.random() * 16);
                System.out.print(cells[x][y] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void setCellValue(int x, int y, int value) {
        cells[x][y] = value;
    }

    public int getCellValue(int x, int y) {
        return cells[x][y];
    }

    public int findLastCellRow() {
        return findLastCellRowCol(FLAG_ROW);
    }

    public int findLastCellCol() {
        return findLastCellRowCol(FLAG_COL);
    }

    private int findLastCellRowCol(int flagRowOrCol) {
        int col = 0;
        int row = 0;
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells[0].length; x++) {
                if (cells[y][x] == PuzzleWindow.LAST_CELL) {
                    row = x;
                    col = y;
                }
            }
        }
        return (flagRowOrCol == FLAG_COL) ? col : row;
    }

    public void swapCells(int x1, int у1, int x2, int y2) {
        int tempCellsValue = getCellValue(x1, у1);
        setCellValue(x1, у1, getCellValue(x2, y2));
        setCellValue(x2, y2, tempCellsValue);
    }

    public int getBoardSize() {
        return (cells == null) ? 0 : cells.length;
    }
}
