package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.model.PuzzleBoard;
import net.javajoy.jps.homework.w12.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class Controller {

    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;

    public Controller(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard  = puzzleBoard ;
        initControllers();
    }

    private void initControllers() {
        puzzleWindow.addBoardKeyListener(new KeyActionListener(puzzleBoard, puzzleWindow));
        puzzleWindow.addMenuActionListener(new MenuActionListener(puzzleBoard, puzzleWindow));
        puzzleWindow.addGameButtonActionListener(new MouseActionListener(puzzleBoard, puzzleWindow));
    }


}
