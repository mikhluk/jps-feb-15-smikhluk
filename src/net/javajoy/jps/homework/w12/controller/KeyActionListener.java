package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.model.PuzzleBoard;
import net.javajoy.jps.homework.w12.view.PuzzleWindow;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class KeyActionListener extends KeyAdapter {

    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;

    public KeyActionListener(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int x = puzzleBoard.findLastCellCol();
        int y = puzzleBoard.findLastCellRow();

        if (e.getKeyCode() == KeyEvent.VK_LEFT && y > 0) {
            puzzleBoard.swapCells(x, y, x, y - 1);
        }else if  (e.getKeyCode() == KeyEvent.VK_RIGHT && y < puzzleBoard.getBoardSize() - 1) {
            puzzleBoard.swapCells(x, y, x, y + 1);
        }else if (e.getKeyCode() == KeyEvent.VK_UP && x > 0) {
            puzzleBoard.swapCells(x, y, x - 1, y);
        }else if (e.getKeyCode() == KeyEvent.VK_DOWN && x < puzzleBoard.getBoardSize() - 1) {
            puzzleBoard.swapCells(x, y, x + 1, y);
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }

    }

}
