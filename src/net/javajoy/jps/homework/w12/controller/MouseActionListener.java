package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.model.PuzzleBoard;
import net.javajoy.jps.homework.w12.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class MouseActionListener implements ActionListener {

    private PuzzleBoard puzzleBoard;
    private PuzzleWindow puzzleWindow;

    public MouseActionListener(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boardButton = (JButton) e.getSource();
        String strValue = boardButton.getActionCommand();

        Integer xClicked = new Integer(strValue) / puzzleBoard.getBoardSize();
        Integer yClicked = new Integer(strValue) % puzzleBoard.getBoardSize();

        checkSwapCells(xClicked, yClicked);
    }

    public void checkSwapCells(int x, int y) {

        //AN: переделать через else-if
        //MS: пока что сколько ни думал, не получилось придумать. либо надо придумывать принципиально другой алгоритм
        // проверки, что клацнули на ячейку, а одна из соседних ячеек пустая. Сейчас логика такая. что мы последовательно
        // берем соседние ячейки, слева, справа, сверху, снизу и проверяем не пустая ли она. Если находим пустую, то
        // меняем ее местами с клацнутой ячейкой. По этому получаются такие вложенные if-ы, иначе будет вылетать
        // ошибка выхода за границы массива


        if (y < puzzleBoard.getBoardSize() - 1) {
            if (puzzleBoard.getCellValue(x, y + 1) == PuzzleWindow.LAST_CELL) {
                puzzleBoard.swapCells(x, y, x, y + 1);
            }
        }

        if (y > 0) {
            if (puzzleBoard.getCellValue(x, y - 1) == PuzzleWindow.LAST_CELL) {
                puzzleBoard.swapCells(x, y, x, y - 1);
            }
        }

        if (x < puzzleBoard.getBoardSize() - 1) {
            if (puzzleBoard.getCellValue(x + 1, y) == PuzzleWindow.LAST_CELL) {
                puzzleBoard.swapCells(x + 1, y, x, y);
            }
        }

        if (x > 0) {
            if (puzzleBoard.getCellValue(x - 1, y) == PuzzleWindow.LAST_CELL) {
                puzzleBoard.swapCells(x - 1, y, x, y);
            }
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }
    }
}

