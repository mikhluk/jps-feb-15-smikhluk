package net.javajoy.jps.homework.w12.controller;

import net.javajoy.jps.homework.w12.model.PuzzleBoard;
import net.javajoy.jps.homework.w12.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class MenuActionListener implements ActionListener {

    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;

    public MenuActionListener(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem menuItem = (JMenuItem) e.getSource();

        if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_RANDOM)) {
            puzzleBoard.fillBoardRandom();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_GATHERED)) {
            puzzleBoard.fillBoardAscending();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_EXIT)) {
            System.exit(0);
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException("Refresh failed", e1);
        }
    }

}
