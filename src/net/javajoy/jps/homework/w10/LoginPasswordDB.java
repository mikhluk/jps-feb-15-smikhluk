package net.javajoy.jps.homework.w10;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Serey Mikhluk
 */
public class LoginPasswordDB {

    private Map<String, String> base;

    public LoginPasswordDB() {

        base = new HashMap<>();
        base.put("sergml", "12345");
        base.put("alexandr", "a12345");
        base.put("roman", "r12345");
        base.put("boris", "b12345");
    }

    public boolean isCorrect(String loginStr, String passwordStr) {

        loginStr = loginStr.toLowerCase();
        return base.get(loginStr) != null && base.get(loginStr).equals(passwordStr);
    }



}
