package net.javajoy.jps.homework.w10;

import net.javajoy.jps.homework.w10.ui.AuthorizationForm;

/**
 * Home task 10
 * See readme.md
 *
 * @author Sergey Mikhluk
 */
public class Main10 {

    public static void main(String[] args) {

        new AuthorizationForm(new LoginPasswordDB());

    }
}
