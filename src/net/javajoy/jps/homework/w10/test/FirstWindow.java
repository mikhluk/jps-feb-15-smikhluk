package net.javajoy.jps.homework.w10.test;

import javax.swing.*;

/**
 * Created by Serg on 18.05.2015.
 */
public class FirstWindow extends JFrame {
    private static final String BACK_IMAGE_PATH = "D:\\0003.jpg" ;

    public FirstWindow() {
        //setResizable(false);

        setTitle("First Window");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        createContent();
        pack();
        setLocationRelativeTo(null);
    }

    private void createContent() {
        ImageIcon imageIcon = new ImageIcon(BACK_IMAGE_PATH);
        JLabel imageLabel = new JLabel(imageIcon);
        JPanel backPanel = new JPanel();
        backPanel.add(imageLabel);

        getContentPane().add(backPanel);
        //JFrame -> contentPane -> JPanel -> JLabel -> ImageIcon (image path)
        
    }

    public void display() {
        setVisible(true);
    }
}
