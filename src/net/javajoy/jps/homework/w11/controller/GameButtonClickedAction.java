package net.javajoy.jps.homework.w11.controller;

import net.javajoy.jps.homework.w11.model.Board;
import net.javajoy.jps.homework.w11.view.BoardWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * @author Sergey Mikhluk.
 */
public class GameButtonClickedAction implements ActionListener {
    private Board board;
    private BoardWindow boardWindow;

    public GameButtonClickedAction(Board board, BoardWindow boardWindow) {
        this.board = board;
        this.boardWindow = boardWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton gameButton = (JButton) e.getSource();
        int iClicked = getIClicked(gameButton);
        int jClicked = getJClicked(gameButton);

        if (boardWindow.getGameMode() == BoardWindow.GameMode.MANUAL) { //manual mode
            board.setCell(iClicked, jClicked, 1 - board.getCell(iClicked, jClicked));
        } else { //reverse mode
            board.fillCellsReverse(iClicked, jClicked);
        }

        boardWindow.repaintGameBoard();
    }


    public static int getIClicked(JButton gameButton) {
        return getClicked(gameButton, 0);
    }

    public static int getJClicked(JButton gameButton) {
        return getClicked(gameButton, 1);
    }

    private static int getClicked(JButton gameButton, int i) {
        String[] strArray = gameButton.getActionCommand().split(BoardWindow.SPACE_SEPARATOR);
        return new Integer(strArray[i]);
    }
}




