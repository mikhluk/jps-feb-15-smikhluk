package net.javajoy.jps.homework.w11.controller;

import net.javajoy.jps.homework.w11.model.Board;
import net.javajoy.jps.homework.w11.view.BoardWindow;

/**
 * @author Sergey Mikhluk.
 */
public class Controller {
    private BoardWindow boardWindow;
    private Board board;

    public Controller(Board board, BoardWindow boardWindow) {
        this.boardWindow = boardWindow;
        this.board = board;
        initListeners();
    }

    private void initListeners() {
        boardWindow.addMenuSelectedAction(new MenuSelectedAction(board, boardWindow));
        boardWindow.addBoardSizeChangedAction(new BoardSizeChangedAction(board, boardWindow));
        boardWindow.addGameButtonClickedAction(new GameButtonClickedAction(board, boardWindow));
    }
}
