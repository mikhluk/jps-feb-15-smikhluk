package net.javajoy.jps.homework.w11.controller;

import net.javajoy.jps.homework.w11.model.Board;
import net.javajoy.jps.homework.w11.view.BoardWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Sergey Mikhluk.
 */
public class BoardSizeChangedAction implements ActionListener {
    public static final int MINIMUM_BOARD_SIZE = 2;
    public static final int MAXIMUM_BOARD_SIZE = 10;
    private Board board;
    private BoardWindow boardWindow;

    public BoardSizeChangedAction(Board board, BoardWindow boardWindow) {
        this.board = board;
        this.boardWindow = boardWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTextField boardSizeField = (JTextField) e.getSource();
        int boardSize = new Integer(boardSizeField.getText());

        if (boardSize < MINIMUM_BOARD_SIZE) {
            boardSize = MINIMUM_BOARD_SIZE;
            boardSizeField.setText(Integer.toString(boardSize));
            System.out.println("Incorrect value! Size of board = 2");
        }
        if (boardSize > MAXIMUM_BOARD_SIZE) {
            boardSize = MAXIMUM_BOARD_SIZE;
            boardSizeField.setText(Integer.toString(boardSize));
            System.out.println("Incorrect value! Size of board = 10");
        }

        board = Board.getInstance(boardSize, true);
        boardWindow.setBoard(board);

        JPanel backPanel = boardWindow.getBackPanel();
        backPanel.remove(boardWindow.getCellPanel());

        boardWindow.setCellPanel(new JPanel(new GridLayout(boardSize, boardSize)));
        boardWindow.createGameGrid();
        backPanel.add(boardWindow.getCellPanel());
        boardWindow.repaintGameBoard();
        boardWindow.addMenuSelectedAction(new MenuSelectedAction(board, boardWindow));
        boardWindow.addGameButtonClickedAction(new GameButtonClickedAction(board, boardWindow));
    }
}
