package net.javajoy.jps.homework.w11.controller;

import net.javajoy.jps.homework.w11.model.Board;
import net.javajoy.jps.homework.w11.view.BoardWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Sergey Mikhluk.
 */
public class MenuSelectedAction implements ActionListener {
    private BoardWindow boardWindow;
    private Board board;

    public MenuSelectedAction(Board board, BoardWindow boardWindow) {
        this.boardWindow = boardWindow;
        this.board = board;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem menuItem = (JMenuItem) e.getSource();

        if (menuItem.getText().equals("Random")) {
            board.fillCellsRandom();
        } else if (menuItem.getText().equals("Manual mode")) {
            boardWindow.setGameMode(BoardWindow.GameMode.MANUAL); // game mode = manual (ручная расстановка ячеек)
        } else if (menuItem.getText().equals("Reverse mode")) {
            boardWindow.setGameMode(BoardWindow.GameMode.REVERSE); // game mode = reverse (реверс ячеек в строке и столбце
        } else if (menuItem.getText().equals("Exit")) {
            System.exit(0);
        }
        boardWindow.repaintGameBoard();
    }

}

