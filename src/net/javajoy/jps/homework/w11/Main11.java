package net.javajoy.jps.homework.w11;

import net.javajoy.jps.homework.w11.controller.Controller;
import net.javajoy.jps.homework.w11.model.Board;
import net.javajoy.jps.homework.w11.view.BoardWindow;

import javax.swing.*;

/**
 * @author Sergey Mikhluk
 */
public class Main11 {

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        System.out.println("Version 5!");
        Board boardInstance = Board.getInstance(Board.INITIAL_BOARD_SIZE, true);
        new Controller(boardInstance, new BoardWindow(boardInstance));
    }

}
