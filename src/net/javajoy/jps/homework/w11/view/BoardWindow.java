package net.javajoy.jps.homework.w11.view;

import net.javajoy.jps.homework.w11.controller.BoardSizeChangedAction;
import net.javajoy.jps.homework.w11.controller.GameButtonClickedAction;
import net.javajoy.jps.homework.w11.controller.MenuSelectedAction;
import net.javajoy.jps.homework.w11.model.Board;

import javax.swing.*;
import java.awt.*;

/**
 * @author Sergey Mikhluk
 */

public class BoardWindow extends JFrame {

    public static final String MAIN_WINDOW_TITLE = "Game Reverse";
    public static final int MAIN_WINDOW_WIDTH = 1000;
    public static final int MAIN_WINDOW_HEIGHT = 600;
    public static final String SPACE_SEPARATOR = " ";

    public enum GameMode {MANUAL, REVERSE}

    public enum CellsColors {
        CYAN(Color.CYAN),
        PINK(Color.PINK);

        private Color color;

        CellsColors(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    private JPanel backPanel;
    private JPanel cellPanel;
    private JPanel statusPanel;
    private JLabel gameModeLabel;
    private JLabel colorNumbersLabel;
    private JLabel gameResultLabel;
    private JTextField boardSizeField;
    private Board board;
    private GameMode gameMode = GameMode.REVERSE;
    private JMenu menu;

    public BoardWindow(Board board) {
        this.board = board;

        createMenu();
        createStatusForm(); //создаем панель статусов в т.ч. устанавливаем размер boardSizeField из INITIAL_BOARD_SIZE
        createGameGrid();
        getContentPane().add(createBackPanel());
        repaintGameBoard();

        configureFrame();
        display();
    }

    private JPanel createBackPanel() {
        backPanel = new JPanel(new GridLayout(1, 2));
        backPanel.add(statusPanel);
        backPanel.add(cellPanel);
        return backPanel;
    }

    private void configureFrame() {
        setTitle(MAIN_WINDOW_TITLE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
    }

    private void createMenu() {
        menu = new JMenu("Menu");

        JMenuItem randomGame = new JMenuItem("Random");
        JMenuItem manualGame = new JMenuItem("Manual mode");
        JMenuItem reverseGame = new JMenuItem("Reverse mode");
        JMenuItem exitGame = new JMenuItem("Exit");

        menu.add(randomGame);
        //menu.addSeparator();  //MS: убрал сепаратор потому что не срабатывает menu.getItem(i).addActionListener(this.menuSelectedAction) . как сделать чтобы сепараторы остались и код работал?
        menu.add(manualGame);
        //menu.addSeparator();
        menu.add(reverseGame);
        //menu.addSeparator();
        menu.add(exitGame);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menu);
        setJMenuBar(menuBar);
    }

    private void createStatusForm() {
        gameModeLabel = new JLabel();
        colorNumbersLabel = new JLabel();
        gameResultLabel = new JLabel();
        boardSizeField = new JTextField();

        statusPanel = new JPanel(new GridLayout(4, 1));
        statusPanel.add(gameModeLabel);
        statusPanel.add(colorNumbersLabel);
        statusPanel.add(gameResultLabel);
        statusPanel.add(boardSizeField);

        boardSizeField.setText(Integer.toString(Board.INITIAL_BOARD_SIZE));
    }

    public void createGameGrid() {
        cellPanel = new JPanel(new GridLayout(getBoardSize(), getBoardSize()));

        for (int x = 0; x < getBoardSize(); x++) {
            for (int y = 0; y < getBoardSize(); y++) {
                final JButton gameButton = new JButton(x + SPACE_SEPARATOR + y);
                gameButton.setActionCommand(x + SPACE_SEPARATOR + y);
                cellPanel.add(gameButton);
            }
        }
    }

    private int getNumberOfComponent(int x, int y) {
        return x * getBoardSize() + y;
    }

    public void repaintGameBoard() {
        repaintCells();
        showGameMode();
    }

    private void showGameMode() {
        gameModeLabel.setText("Mode = " + gameMode.name());
    }

    private void repaintCells() {
        int firstColor = 0;
        int secondColor = 0;

        for (int x = 0; x < getBoardSize(); x++) {
            for (int y = 0; y < getBoardSize(); y++) {
                JButton jButtonTemp = (JButton) cellPanel.getComponent(getNumberOfComponent(x, y));
                jButtonTemp.setBackground(board.getCell(x, y) == 0 ? CellsColors.CYAN.getColor() : CellsColors.PINK.getColor());

                if (board.getCell(x, y) == 0) {
                    firstColor++;
                } else {
                    secondColor++;
                }

                colorNumbersLabel.setText(String.format("%s = %d, %s = %d", CellsColors.CYAN, firstColor, CellsColors.PINK, secondColor));
            }
        }
        updateGameStatus(firstColor, secondColor);
    }

    private void updateGameStatus(int firstColor, int secondColor) {
        if ((firstColor == 0) || (secondColor == 0)) {
            gameResultLabel.setText(" You won!!");
        } else {
            gameResultLabel.setText(" Play!");
        }
    }

    public void display() {
        setVisible(true);
    }

    public JPanel getBackPanel() {
        return backPanel;
    }

    public JPanel getCellPanel() {
        return cellPanel;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public int getBoardSize() {
        return new Integer(boardSizeField.getText());
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setCellPanel(JPanel cellPanel) {
        this.cellPanel = cellPanel;
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }


    public void addMenuSelectedAction(MenuSelectedAction menuSelectedAction) {
        for (int i = 0; i < menu.getItemCount(); i++) {
            menu.getItem(i).addActionListener(menuSelectedAction);
        }
    }

    public void addGameButtonClickedAction(GameButtonClickedAction gameButtonClickedAction) {
        for (int x = 0; x < getBoardSize(); x++) {
            for (int y = 0; y < getBoardSize(); y++) {
                JButton cellButton = (JButton) cellPanel.getComponent(getNumberOfComponent(x, y));
                cellButton.addActionListener(gameButtonClickedAction);
            }
        }
    }

    public void addBoardSizeChangedAction(BoardSizeChangedAction boardSizeChangedAction) {
        boardSizeField.addActionListener(boardSizeChangedAction);
    }
}