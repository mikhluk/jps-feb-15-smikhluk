package net.javajoy.jps.homework.w11.model;

/**
 * @author Sergey Mikhluk
 */

public class Board {
    public static final int INITIAL_BOARD_SIZE = 3;

    private static Board instance;
    private int cells[][];
    private int boardSize;

    private Board(int boardSize) {
        this.cells = new int[boardSize][boardSize];
        this.boardSize = boardSize;
    }

    public static Board getInstance(int boardSize, boolean createNewInstance) {
        if (createNewInstance) {
            instance = new Board(boardSize);
        }
        return instance;
    }

    public int getCell(int x, int y) {
        return cells[x][y];
    }

    public void setCell(int x, int y, int value) {
        cells[x][y] = value;
    }

    public void fillCellsRandom() {
        for (int x = 0; x < boardSize; x++) {
            for (int y = 0; y < boardSize; y++) {
                cells[x][y] = (int) (Math.random() * 2);
            }
        }
    }

    public void fillCellsReverse(int x, int y) {
        for (int index = 0; index < boardSize; index++) {
            cells[x][index] = 1 - cells[x][index];
            if (index != x) {
                cells[index][y] = 1 - cells[index][y];
            }
        }
    }
}
