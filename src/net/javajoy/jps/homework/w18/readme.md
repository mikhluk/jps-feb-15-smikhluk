Webinar 18 - Home Task
1. Напишите приложение, создающее крупные объекты по мере ввода данныхи проследите за использованием памяти
с помощью jvisualvm.
Поэкспериментируйте с количеством объектов, ссылки на которые удаляются.
Как влияет их количество назаполнение сегментов Survivor и Old Generation?

  -- https://gyazo.com/dea9a31132fc91095e8e5b46bb032ea0
  -- Пока создаются новые объекты, то Old Generation и Survivor очищаются спустя несколько секунд после того как
  -- удалились ссылки на неиспользуемые объекты. Когда программа переходит в режим "сна", то ссылки на неиспользуемые
  -- объекты удаляются через несколько минут.

2. Добейтесь заполнения всего пространства heap. Что будет, если продолжить создавать объекты?
  -- Exception in thread "main" java.lang.OutOfMemoryError: Java heap space


3. Запакуйте все классы, созданные на предыдущем занятии в один .jar-архив.

*4. Сделайте один из методов вашего класса нативным (опционально).

Последнее изменение: Пятница, 3 Июль 2015, 16:29