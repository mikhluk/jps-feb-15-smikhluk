package net.javajoy.jps.homework.w18;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public class Main18 {
    private List<byte[]> bytes;
    private List<byte[]> bytes1;
    private List<byte[]> bytes2;
    private List<byte[]> bytes3;

    public static void main(String[] args) throws InterruptedException {
        Main18 main = new Main18();


    }

    public Main18() throws InterruptedException {
        bytes = new ArrayList<>();
        bytes1 = new ArrayList<>();
        bytes2 = new ArrayList<>();
        bytes3 = new ArrayList<>();

        consume();
        release();


    };

    private void consume() throws InterruptedException {
        for (int i = 0; i < 200; i ++){
            bytes.add(new byte[1024*1024]);
            Thread.sleep(100);
            System.out.printf("%d bytes Memory consumption .. %n", i);
        }
        bytes = null;

        for (int i = 0; i < 200; i ++){
            bytes1.add(new byte[1024*1024]);
            Thread.sleep(100);
            System.out.printf("%d bytes1 Memory consumption .. %n", i);
        }
        //bytes1 = null;

        for (int i = 0; i < 200; i ++){
            bytes2.add(new byte[1024*1024]);
            Thread.sleep(100);
            System.out.printf("%d bytes2 Memory consumption .. %n", i);
        }
        bytes2 = null;


        for (int i = 0; i < 300; i ++){
            bytes3.add(new byte[1024*1024]);
            Thread.sleep(100);
            System.out.printf("%d bytes2 Memory consumption .. %n", i);
        }
        bytes3 = null;




    }

    private void release() throws InterruptedException {
        bytes = null;
        while (true) {
            Thread.sleep(1_000);
            System.out.println("Sleeping");
        }
    }
}
