package net.javajoy.jps.homework.w16;

import net.javajoy.jps.homework.w16.controller.Controller;
import net.javajoy.jps.homework.w16.view.FriendsForm;

import java.util.Vector;

/**
 * @author Sergey Mikhluk.
 */
public class FriendsMain16 {

    public static void main(String[] args) {
        Vector<String> defaultNameList = new Vector<>();
        defaultNameList.add("Andrew");
        defaultNameList.add("Sergey");
        defaultNameList.add("Anton");
        defaultNameList.add("Dmitry");

        new Controller(new FriendsForm(defaultNameList));
    }
}
