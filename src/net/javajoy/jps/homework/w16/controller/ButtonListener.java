package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.view.FriendsForm;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Paths;
import java.util.Vector;

/**
 * @author Sergey Mikhluk.
 */
public class ButtonListener implements ActionListener {
    private FriendsForm form;

    public ButtonListener(FriendsForm form) {
        this.form = form;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();
        String operation = button.getActionCommand();

        if (operation.equals(FriendsForm.Operation.COPY_TO_RIGHT.getName())) {
            copyItem(form.getJListLeft(), form.getRightNameList(), form.getLeftNameList(), true);

        } else if (operation.equals(FriendsForm.Operation.COPY_TO_LEFT.getName())) {
            copyItem(form.getJListRight(), form.getLeftNameList(), form.getRightNameList(), false);

        } else if (operation.equals(FriendsForm.Operation.SAVE.getName())) {
            JFileChooser fileChooser = createFileChooser();
            int confirmation = fileChooser.showSaveDialog(null);
            saveOrLoad(fileChooser, confirmation, true);

        } else if (operation.equals(FriendsForm.Operation.LOAD.getName())) {
            JFileChooser fileChooser = createFileChooser();
            int confirmation = fileChooser.showOpenDialog(null);
            saveOrLoad(fileChooser, confirmation, false);
        }
    }

    private void copyItem(JList jListFrom, Vector<String> nameListTo, Vector<String> nameListFrom, boolean flagToRight) {
        Object selectedItem = jListFrom.getSelectedValue();
        if (selectedItem != null) {
            nameListTo.add((String) selectedItem);
            nameListFrom.remove(jListFrom.getSelectedIndex());

            if (flagToRight) {
                form.setJListRight(nameListTo);
                form.setJListLeft(nameListFrom);
            } else {
                form.setJListRight(nameListFrom);
                form.setJListLeft(nameListTo);
            }
        }
    }

    private void saveOrLoad(JFileChooser fileChooser, int confirmation, boolean flagSave) {
        if (confirmation == JFileChooser.APPROVE_OPTION) {
            try {
                if (flagSave) {
                    saveGameToFile(fileChooser.getSelectedFile().getName());
                } else {
                    loadGameFromFile(fileChooser.getSelectedFile().getName());
                }
            } catch (IOException | ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        }
    }

    private JFileChooser createFileChooser() {
        return new JFileChooser(Paths.get(".").toFile());
    }

    public void saveGameToFile(String fileName) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(form.getLeftNameList());
            oos.writeObject(form.getRightNameList());
        }
    }

    @SuppressWarnings("unchecked")
    public void loadGameFromFile(String fileName) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ios = new ObjectInputStream(new FileInputStream(fileName))) {

            Object loadedItem = ios.readObject();
            if (loadedItem.getClass().equals(Vector.class)) {
                Vector<String> nameList = (Vector<String>) loadedItem;
                form.setLeftNameList(nameList);
                form.setJListLeft(nameList);
            }

            loadedItem = ios.readObject();
            Vector<String> nameList = (Vector<String>) loadedItem;
            if (loadedItem.getClass().equals(Vector.class)) {
                form.setRightNameList(nameList);
                form.setJListRight(nameList);
            }
        }
    }
}
