package net.javajoy.jps.homework.w16.controller;

import net.javajoy.jps.homework.w16.view.FriendsForm;

/**
 * @author Sergey Mikhluk.
 */
public class Controller {
    private FriendsForm form;

    public Controller(FriendsForm form) {
        this.form = form;
        initListeners();
    }

    private void initListeners() {
        form.addButtonListener(new ButtonListener(form));
    }

}
