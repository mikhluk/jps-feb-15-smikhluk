package net.javajoy.jps.homework.w03;

public class Time {
    public static final int MAX_MINUTE = 60;
    public static final int MAX_SECOND = 60;

    private int hour;
    private int minute;
    private int second;

    public Time() { //конструктор по умолчанию
    }

    public Time(int hh, int mm, int ss) {  //конструктор с параметрами
        hour = hh;
        minute = mm;
        second = ss;
    }

    public Time(Time t) {  //копирующий конструктор
        this.hour = t.hour;
        this.minute = t.minute;
        this.second = t.second;
    }

    //переопределяем метод toString
    public String toString() {
        return String.format("%02d:%02d:%02d", this.getHour(), this.getMinute(), this.getSecond());
    }

    //геттеры и сеттеры
    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public void setHour(int hh) {
        this.hour = hh;
    }

    public void setMinute(int mm) {
        this.minute = mm;
    }

    public void setSecond(int ss) {
        this.second = ss;
    }

    // Статический метод создания объекта Time
    public static Time createTime(int hour, int minute, int second) {
        return new Time(hour, minute, second);
    }

    public static int timeInSecond(Time time) {
        return time.getSecond() + (time.getMinute() * MAX_SECOND) + (time.getHour() * MAX_MINUTE * MAX_SECOND);
    }

    // Статический метод проверки на больше меньше
    // В качестве параметро два аргумента типа Time
    public static int compareTimes(Time time1, Time time2) {
        int timeInSecond1 = timeInSecond(time1);
        int timeInSecond2 = timeInSecond(time2);

        if (timeInSecond1 > timeInSecond2) {
            return 1;
        } else if (timeInSecond1 < timeInSecond2) {
            return -1;
        }
        return 0;
    }

    // нестатический метод, выводит разницу во времени в секундах
    public int minusTime(Time time1, Time time2) {
        return timeInSecond(time1) - timeInSecond(time2);
    }

    public void printTime() {
        System.out.println("Время: " + hour + ":" + minute + ":" + second);
    }

}





