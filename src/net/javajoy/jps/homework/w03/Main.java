package net.javajoy.jps.homework.w03;

import java.util.Scanner;

/**
 * Webinar 3 - Home Task
 * В классе "Время" из ДЗ 02, переопределите метод toString() для удобного вывода состояния объекта на печать.
 * <p/>
 * Создайте производный от него класс, в котором:
 * - добавьте метод определения времени дня. Результатом такого метода должен являться экземпляр перечисления вида {«утро»,«день»,«вечер»,«ночь»};
 * - переопределите метод toString() для вывода времени в текстовом виде, например
 * «десятьчасов : двадцать пять минут» вместо «10:25». По желанию можете добавить также вывод секунд.
 * <p/>
 * В перечислении («утро», «день», «вечер», «ночь») сохраните время начала и конца каждого периода. Реализуйте в этом перечислении конструктор и методы для соответствующих полей.
 * <p/>
 * Продемонстрируйте создание нескольких экземпляров для каждого из классов и вывод их состояния на печать.
 */

public class Main {
    // определяем константы, количество часов в сутках, минут в часах, секунд в минутах
    public static final int MAX_HOUR = 24;
    public static final int MAX_MINUTE = 60;
    public static final int MAX_SECOND = 60;

    // определяем массив с максимальными значениями часов минут секунд
    public static final int[] MAX_HMS = {MAX_HOUR, MAX_MINUTE, MAX_SECOND};

    // константа показывающая сколько ячеек будет в массивах хранящих данных о времени, равна 3 т.к. храним часы, минуты, секунды
    public static final int TIME_DIMENSION = 3;
    public static final int NUMBER_OF_EXAMPLES = 2; // константа показывающая экземпляров времени с консоли должен ввести пользователь

    public static void scanTime(int hmsArray[]) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите время в формате чч мм сс (через пробел)");

        boolean errorFound;
        String inputString;
        do {  // цикл, запрашиваем ввод времени, пока не будет введено корректное значение чч мм сс
            errorFound = false;
            for (int i = 0; i < hmsArray.length; i++) {
                while (true) {  //зациклили ввод, пока не будет введено число
                    scanner.hasNext();
                    inputString = scanner.next();
                    if (inputString.matches("[0-9]*")) {
                        break;
                    }
                    i = 0;
                    System.out.println("Введено не число. Повторите ввод");
                }

                hmsArray[i] = Integer.valueOf(inputString);
                if ((hmsArray[i] >= MAX_HMS[i]) || (hmsArray[i] < 0)) {
                    System.out.println("Введено некорректное число. Повторите ввод");
                    errorFound = true;
                }
            }
        } while (errorFound);
    }

    public static void main(String[] args) {
        printPartOfDayElement(PartOfDay.NIGHT);
        printPartOfDayElement(PartOfDay.MORNING);
        printPartOfDayElement(PartOfDay.AFTERNOON);
        printPartOfDayElement(PartOfDay.EVENING);

        int[] hmsArray = new int[TIME_DIMENSION];

        System.out.println("Вам понадобится ввести данные о времени " + NUMBER_OF_EXAMPLES + " раза.");

        UpgradedTime uTime;
        for (int i = 0; i < NUMBER_OF_EXAMPLES; i++) {
            // создаем и выводим на печать объект класса Time
            System.out.print("№ " + (i + 1) + ". ");
            scanTime(hmsArray);
            Time t1 = Time.createTime(hmsArray[0], hmsArray[1], hmsArray[2]);
            System.out.println("Время = " + t1);

            // создаем и выводим на печать 1 объект класса UpgradedTime
            uTime = new UpgradedTime(t1);
            System.out.println(uTime + " - " + uTime.getPartOfDay());
        }
    }

    public static void printPartOfDayElement(PartOfDay partOfDay) {
        System.out.printf("%s = с %s по %s%n", partOfDay, partOfDay.getTimeBegin(), partOfDay.getTimeEnd());
    }
}
