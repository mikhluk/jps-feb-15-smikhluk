package net.javajoy.jps.homework.w03;

/**
 * Created by Serg on 20.03.2015.
 */
public enum PartOfDay {
    NIGHT(new Time(0, 0, 0), new Time(3, 59, 59)),
    MORNING(new Time(4, 0, 0), new Time(11, 59, 59)),
    AFTERNOON(new Time(12, 0, 0), new Time(16, 59, 59)),
    EVENING(new Time(17, 0, 0), new Time(23, 59, 59));

    private Time timeBegin;
    private Time timeEnd;

    PartOfDay(Time t1, Time t2) {
        timeBegin = t1;
        timeEnd = t2;
    }

    public Time getTimeBegin() {
        return timeBegin;
    }

    public Time getTimeEnd() {
        return timeEnd;
    }
}

