package net.javajoy.jps.homework.w03.testWithString;

/**
 * Created by Serg on 20.03.2015.
 */
public class UpgradedTime extends Time {
//    private enum partOfTime;

    public String partOfTime = "";


//    public String getPartOfTime() {
//
////        if (this.getHour()< 4){
////            return PartOfDay.NIGHT;
////        }
////        else if (this.getHour() < 12) {
////            return PartOfDay.MORNING;
////        }
////        else if (this.getHour()< 17){
////            return PartOfDay.AFTERNOON;
////        }
////        else if (this.getHour()< 24){
////            return PartOfDay.EVENING;
////        }
//
//        //потестируем String т.к. enum не работает
//
//        return s;

//    }


    public UpgradedTime(byte hh, byte mm, byte ss) {
        super(hh, mm, ss);
        String s = "";
        if (this.getHour() < 4) {
            s = "NIGHT";
        } else if (this.getHour() < 12) {
            s = "MORNING";
        } else if (this.getHour() < 17) {
            s = "AFTERNOON";
        } else if (this.getHour() < 24) {
            s = "EVENING";
        }
        this.partOfTime = s;
    }

    public UpgradedTime(Time t) {
        super(t);
        String s = "";
        if (this.getHour() < 4) {
            s = "NIGHT";
        } else if (this.getHour() < 12) {
            s = "MORNING";
        } else if (this.getHour() < 17) {
            s = "AFTERNOON";
        } else if (this.getHour() < 24) {
            s = "EVENING";
        }
        this.partOfTime = s;
    }

    public String getPartOfTime() {
        return partOfTime;
    }

    public void setPartOfTime() {
        this.partOfTime = "";
    }

//    public String toString(){
//        if (getPartOfTime() = PartOfDay.NIGHT) return "Night";
//        if (getPartOfTime() = PartOfDay.MORNING) return "Morningt";
//        if (getPartOfTime() = PartOfDay.AFTERNOON) return "Afternoon";
//        if (getPartOfTime() = PartOfDay.EVENING) return "Evening";
//    }
}
