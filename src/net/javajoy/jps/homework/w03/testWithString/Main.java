package net.javajoy.jps.homework.w03.testWithString;

import java.util.Scanner;

public class Main {

    public static void timeScanner(byte a[], String s) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(s);
        for (int i = 0; i < 3; i++) {
            if (scanner.hasNextByte()) {
                a[i] = scanner.nextByte();
            }
        }
        if (a[0] > 23) {
            a[0] = 0;
            System.out.println("Введено некорректное число часов");
        }
        if (a[1] > 59) {
            a[1] = 0;
            System.out.println("Введено некорректное число минут");
        }
        if (a[2] > 59) {
            a[2] = 0;
            System.out.println("Введено некорректное число секунд");
        }
    }

    // AS: могу вводить значения вне диапазона, например 55 22 33,
    // а также буквы


    private static void demoScannerMiniTest() {
        Scanner scanner = new Scanner(System.in).useDelimiter("[,%n]+");

        for (int i = 0; i < 3; i++) {
            if (scanner.hasNextInt()) {
                System.out.print(scanner.nextInt() + " ");
            }
        }
    }


    public static void main(String[] args) {

        demoScannerMiniTest();


        Time t1 = new Time((byte) 11, (byte) 20, (byte) 30); // создаем объект t1 через конструктор с параметрами чч мм сс
        System.out.println("Время 1 =" + t1);

        UpgradedTime uTime = new UpgradedTime(t1);  // протестировать!
        System.out.println(uTime.getPartOfTime());

        t1.setSecond((byte) 45);
        System.out.println("изменили секунды на 45, проверим что копирующий конструктор скопирует последнеее время с 45 секундами");
        System.out.println("Время 1= " + t1);

        //uTime = (UpgradedTime)t1;
        System.out.println(uTime.getPartOfTime());

        Time t2 = new Time(t1);  // создаем новый объект через копирующий конструктор
        System.out.println("Время 2 = " + t2);

        Time t3 = Time.createTimeObject((byte) 2, (byte) 20, (byte) 45);  //создаем новый объект через статический метод создания объекта
        System.out.println("Время 3 = " + t3);

        Time.check2Times(t1, t3);  //сравниваем время на больше меньше
        System.out.println("разница во времени в секундах = " + t1.minusTime(t1, t3));

        byte[] a = new byte[3];

        timeScanner(a, "Введите время 1 в формате чч мм сс (через пробел)");
        Time t4 = Time.createTimeObject(a[0], a[1], a[2]);
        System.out.println("Время 4 = " + t4);
        uTime = new UpgradedTime(t4);
        System.out.println(uTime.getPartOfTime());

        timeScanner(a, "Введите время 2 в формате чч мм сс (через пробел)");
        Time t5 = Time.createTimeObject(a[0], a[1], a[2]);

        System.out.println("Время 5 = " + t5);
        uTime = new UpgradedTime(t5);
        System.out.println(uTime.getPartOfTime());

        Time.check2Times(t4, t5);
        System.out.println("разница во времени в секундах = " + t4.minusTime(t4, t5));


        //AN: закоментированный код перед сдачей на проверку нужно удалить.

//        //фрагмент из дз w02 сравниваем 2 времени и выводим разницу в секундах
//        // код рабочий можно расскомментировать
//        t1 = new Time(11, 59, 59);
//        System.out.println(t1);
//        t2 = new Time(12, 01, 01);
//        System.out.println(t2);
//        switch (Time.compareTimes(t1, t2)) {
//            case -1:
//                System.out.println("Time 1 < Time 2");
//                break;
//            case 0:
//                System.out.println("Time 1 = Time 2");
//                break;
//            case 1:
//                System.out.println("Time 1 > Time 2");
//                break;
//        }
//
//        System.out.println("разница во времени в секундах = " + t1.minusTime(t1, t2));


    }

}
