package net.javajoy.jps.homework.w03;

public class UpgradedTime extends Time {
    public PartOfDay partOfDay;

    public UpgradedTime(int hh, int mm, int ss) { // конструктор объекта по трем числам чч мм сс + enum автовычисление времени суток
        super(hh, mm, ss);
        definePartOfDay();
    }

    public UpgradedTime(Time t) {// конструктор объекта на основании параметра объекта Time + enum автовычисление времени суток
        super(t);
        definePartOfDay();
    }

    public PartOfDay getPartOfDay() { // геттер - возвращает значение времени суток  enum (ночь, утро, день, вечер)
        return partOfDay;
    }

    public void definePartOfDay() { // сеттер - устанавливает значение времени суток  enum (ночь, утро, день, вечер)
        if (this.getHour() < 4) {
            partOfDay = PartOfDay.NIGHT;
        } else if (this.getHour() < 12) {
            partOfDay = PartOfDay.MORNING;
        } else if (this.getHour() < 17) {
            partOfDay = PartOfDay.AFTERNOON;
        } else if (this.getHour() < 24) {
            partOfDay = PartOfDay.EVENING;
        }
    }

    public static String translateDigitToString(int digit) {
        String dString = "";
        if (digit >= 0 && digit < 20) {
            switch (digit) {
                case 0:
                    dString = "ноль";
                    break;
                case 1:
                    dString = "один";
                    break;
                case 2:
                    dString = "два";
                    break;
                case 3:
                    dString = "три";
                    break;
                case 4:
                    dString = "четыре";
                    break;
                case 5:
                    dString = "пять";
                    break;
                case 6:
                    dString = "шесть";
                    break;
                case 7:
                    dString = "семь";
                    break;
                case 8:
                    dString = "восемь";
                    break;
                case 9:
                    dString = "девять";
                    break;
                case 10:
                    dString = "десять";
                    break;
                case 11:
                    dString = "одиннадцать";
                    break;
                case 12:
                    dString = "двенадцать";
                    break;
                case 13:
                    dString = "тринадцать";
                    break;
                case 14:
                    dString = "четырнадцать";
                    break;
                case 15:
                    dString = "пятнадцать";
                    break;
                case 16:
                    dString = "шестнадцать";
                    break;
                case 17:
                    dString = "семнадцать";
                    break;
                case 18:
                    dString = "восемнадцать";
                    break;
                case 19:
                    dString = "девятнадцать";
                    break;
            }
        } else if (digit > 20 && digit < 100) {
            switch (digit / 10) {
                case 2:
                    dString = "двадцать";
                    break;
                case 3:
                    dString = "тридцать";
                    break;
                case 4:
                    dString = "сорок";
                    break;
                case 5:
                    dString = "пятьдесят";
                    break;
                case 6:
                    dString = "шестьдесят";
                    break;
                case 7:
                    dString = "семьдесят";
                    break;
                case 8:
                    dString = "восемьдесят";
                    break;
                case 9:
                    dString = "девяносто";
                    break;
            }
            dString = dString + " " + translateDigitToString(digit % 10);
        }
        return dString;
    }

    public String toString() {  // работает, не хватает только склонения два - две, часа - часов и т.д.
        return String.format("%s часов: %s минут: %s секунд",
                translateDigitToString(getHour()), translateDigitToString(getMinute()), translateDigitToString(getSecond()));
    }
}
