package net.javajoy.jps.homework.w07;

import java.util.*;

/**
 * Created by Serg on 10.05.2015.
 */
public class MiniTest07 {

    static void miniTestList() {
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new LinkedList<>();
        fillCollection(list1, 5);
        fillCollection(list2, 5);

        Collection<String> concatenatedCollection = concat(list1, list2);
        System.out.println(concatenatedCollection.size());
        System.out.println(concatenatedCollection);

    }

    private static Collection<String> concat(Collection<String> list1, Collection<String> list2) {
        Collection<String> result = new ArrayList<>(list1);
        result.addAll(list2);
        return result;
    }

    private static void fillCollection(Collection<String> collection, int n) {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            collection.add("E-" + random.nextInt(200));
        }
    }

    public static void main(String[] args) {
        miniTestList();

    }

}
