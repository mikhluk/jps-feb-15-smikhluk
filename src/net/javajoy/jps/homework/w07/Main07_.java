package net.javajoy.jps.homework.w07;

import net.javajoy.jps.homework.w06.StringUtils;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Home task 07
 * See readme.md
 *
 * @author Sergey Mikhluk
 */
public class Main07_ {

    // AS: ������ ���� �� ����������� ��� ����� ��������. � ����, ��� ��� ����� ������ �������
    // ������������� ��������� ����� � �����, �� ���������� ������ � ������ �� �������� ���������
    // StringUtils - ����������
    public static void testBracketsWithStack() throws Exception {

        Deque<String> strStack = new ArrayDeque<String>();

        String testString = "(a + b) = {c}";
        System.out.println("Checked string: " + testString);

        for (char c : testString.toCharArray()) {
            if (StringUtils.isBracket(c)) {
                if (StringUtils.isPairBracket(String.valueOf(c), strStack.peek())) {
                    strStack.pop();
                } else {
                    strStack.push(String.valueOf(c));
                }
            }
        }

        System.out.printf("Stack content is %s, result: %s%n",
                strStack, strStack.size() == 0 ? "OK" : "WRONG");

    }


    public static void main(String[] args) throws Exception{

        System.out.println("\n================================== Check placement of brackets using the STACK =================================");
        testBracketsWithStack();

    }

    // AS: ����� 2 � 3 ���������?
}
