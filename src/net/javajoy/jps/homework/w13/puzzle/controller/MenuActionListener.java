package net.javajoy.jps.homework.w13.puzzle.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JColorChooser;
import javax.swing.JMenuItem;

import net.javajoy.jps.homework.w13.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w13.puzzle.view.HelpWindow;
import net.javajoy.jps.homework.w13.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class MenuActionListener implements ActionListener {

    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;

    public MenuActionListener(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem menuItem = (JMenuItem) e.getSource();

        if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_RANDOM)) {
            puzzleBoard.fillBoardRandom();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_GATHERED)) {
            puzzleBoard.fillBoardAscending();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_BACKGROUND_COLOR)) {
            puzzleWindow.setBackgroundColor(JColorChooser.showDialog(null, "Choose a background color", null));
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_BORDER_COLOR)) {
            puzzleWindow.setBorderColor(JColorChooser.showDialog(null, "Choose a border color", null));
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_HELP)) {
            new HelpWindow();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_EXIT)) {
            System.exit(0);
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException("Refresh failed", e1);
        }
    }

}
