package net.javajoy.jps.homework.w13.puzzle.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import net.javajoy.jps.homework.w13.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w13.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class Controller {

    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;

    public Controller(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
        initControllers();
    }

    private void initControllers() {
        puzzleWindow.addBoardKeyListener(new KeyActionListener(puzzleBoard, puzzleWindow));
        puzzleWindow.addMenuActionListener(new MenuActionListener(puzzleBoard, puzzleWindow));
        puzzleWindow.addGameFieldListener(new MouseActionListener(puzzleBoard, puzzleWindow));

        puzzleWindow.getStatusPanel().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    puzzleWindow.displayPopupMenu(e.getXOnScreen(), e.getYOnScreen());
                }
            }
        });

    }

}
