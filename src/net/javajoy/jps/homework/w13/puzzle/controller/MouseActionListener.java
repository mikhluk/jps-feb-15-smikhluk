package net.javajoy.jps.homework.w13.puzzle.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

import net.javajoy.jps.homework.w13.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w13.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class MouseActionListener implements ActionListener {

    private PuzzleBoard puzzleBoard;
    private PuzzleWindow puzzleWindow;

    public MouseActionListener(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boardButton = (JButton) e.getSource();
        String buttonIndex = boardButton.getActionCommand();

        Integer xClicked = new Integer(buttonIndex) / puzzleBoard.getBoardSize();
        Integer yClicked = new Integer(buttonIndex) % puzzleBoard.getBoardSize();

        checkSwapCells(xClicked, yClicked);
    }

    public void checkSwapCells(int x, int y) {
        //AN: почему не используешь else if? Тут ведь только один из if выражений всегда срабатывает
        //AN: почему не объденишь внешний и вложенный if в один?
        //SM: потому что будет ошибка выход за границы массива
        if (y < puzzleBoard.getBoardSize() - 1) {
            if (puzzleBoard.getCellValue(x, y + 1) == PuzzleWindow.EMPTY_CELL) {
                puzzleBoard.swapCells(x, y, x, y + 1);
            }
        }

        if (y > 0) {
            if (puzzleBoard.getCellValue(x, y - 1) == PuzzleWindow.EMPTY_CELL) {
                puzzleBoard.swapCells(x, y, x, y - 1);
            }
        }

        if (x < puzzleBoard.getBoardSize() - 1) {
            if (puzzleBoard.getCellValue(x + 1, y) == PuzzleWindow.EMPTY_CELL) {
                puzzleBoard.swapCells(x + 1, y, x, y);
            }
        }

        if (x > 0) {
            if (puzzleBoard.getCellValue(x - 1, y) == PuzzleWindow.EMPTY_CELL) {
                puzzleBoard.swapCells(x - 1, y, x, y);
            }
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
    }
}

