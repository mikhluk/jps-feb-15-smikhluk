package net.javajoy.jps.homework.w13.puzzle;

import javax.swing.UIManager;

import net.javajoy.jps.homework.w13.puzzle.controller.Controller;
import net.javajoy.jps.homework.w13.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w13.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
/*
AN:

1)не хватает
- «выбор цвета рамки выделения»,
- «справка».

2) Логическая ошибка (простая): меню Gathered открывает BackGroundColor диалог
 */
public class MainPuzzle13 {
    public static final int INITIAL_BOARD_SIZE = 4;

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        PuzzleBoard puzzleBoard = new PuzzleBoard(INITIAL_BOARD_SIZE);
        new Controller(puzzleBoard, new PuzzleWindow(puzzleBoard));
    }

}
