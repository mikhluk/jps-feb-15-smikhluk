package net.javajoy.jps.homework.w13.puzzle.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.javajoy.jps.homework.w13.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class PuzzleBoard {
    public static final int FLAG_COL = 1;
    public static final int FLAG_ROW = 0;

    private int cells[][];

    public PuzzleBoard(int boardSize) {
        this.cells = new int[boardSize][boardSize];
    }

    public void fillBoardAscending() {
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells.length; x++) {
                cells[y][x] = y * cells.length + x;
            }
        }
    }

    public void fillBoardRandom() {
        List<Integer> randomList = new ArrayList<>();
        for (int i = 0; i < cells.length * cells.length; i++) {
            randomList.add(i);
        }
        Collections.shuffle(randomList);

        int index = 0;
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells.length; x++) {
                cells[y][x] = (int) randomList.get(index++);
            }
        }
    }

    public void setCellValue(int x, int y, int value) {
        cells[x][y] = value;
    }

    public int getCellValue(int x, int y) {
        return cells[x][y];
    }

    public int findLastCellRow() {
        return findLastCellRowCol(FLAG_ROW);
    }

    public int findLastCellCol() {
        return findLastCellRowCol(FLAG_COL);
    }

    private int findLastCellRowCol(int flagRowOrCol) {
        int col = 0;
        int row = 0;

        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells.length; x++) {
                if (cells[y][x] == PuzzleWindow.EMPTY_CELL) {
                    row = x;
                    col = y;
                }
            }
        }

        return (flagRowOrCol == FLAG_COL) ? col : row;
    }

    public void swapCells(int x1, int y1, int x2, int y2) {
        int tempCellsValue = getCellValue(x1, y1);
        setCellValue(x1, y1, getCellValue(x2, y2));
        setCellValue(x2, y2, tempCellsValue);
    }

    public int getBoardSize() {
        return (cells == null) ? 0 : cells.length;
    }
}
