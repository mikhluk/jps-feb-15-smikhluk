package net.javajoy.jps.homework.w13.puzzle.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;


/**
 * @author Sergey Mikhluk.
 */
public class HelpWindow extends JFrame {

    public static final String MAIN_WINDOW_TITLE = "Help";

    public static final int MAIN_WINDOW_WIDTH = 300;
    public static final int MAIN_WINDOW_HEIGHT = 200;


    public HelpWindow() {
        super(MAIN_WINDOW_TITLE);
        createMainFrame();
        display();
    }

    private void createMainFrame() {
        JPanel mainPanel;

        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.red));

        mainPanel.add(createHelpTextPane());

        getContentPane().add(mainPanel, BorderLayout.CENTER);
        setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private JTextPane createHelpTextPane() {
        DefaultStyledDocument document = new DefaultStyledDocument();
        SimpleAttributeSet attributes = new SimpleAttributeSet();
        attributes.addAttribute(StyleConstants.Alignment, new Integer(StyleConstants.ALIGN_CENTER));
        String strHelp = "Puzzle Game!\nVersion 1.00\n21.05.2015\nAuthor Sergey M.\nAll rights reserved!";
        try {
            document.insertString(0, strHelp, attributes);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        document.setParagraphAttributes(0, strHelp.length(), attributes, true);
        JTextPane textPaneHelp = new JTextPane();
        textPaneHelp.setStyledDocument(document);
        textPaneHelp.setEditable(false);
        return textPaneHelp;
    }

    public void display() {
        setVisible(true);
    }

}
