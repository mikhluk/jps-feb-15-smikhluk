package net.javajoy.jps.homework.w13.puzzle.view;
import net.javajoy.jps.homework.w13.puzzle.controller.RandomAction;
import net.javajoy.jps.homework.w13.puzzle.model.PuzzleBoard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import net.javajoy.jps.homework.w13.puzzle.controller.KeyActionListener;
import net.javajoy.jps.homework.w13.puzzle.controller.MenuActionListener;
import net.javajoy.jps.homework.w13.puzzle.controller.MouseActionListener;


/**
 * @author Sergey Mikhluk
 */
public class PuzzleWindow extends JFrame {

    public static final String MAIN_WINDOW_TITLE = "Puzzle window";
    public static final String RESOURCE_PATH = "resources/w13";
    public static final int MAIN_WINDOW_WIDTH = 680;
    public static final int MAIN_WINDOW_HEIGHT = 580;
    public static final int EMPTY_CELL = 15;

    public static final String MENU_RANDOM = "Random";
    public static final String MENU_GATHERED = "Gathered";
    public static final String MENU_BACKGROUND_COLOR = "Background Color";
    public static final String MENU_BORDER_COLOR = "Border Color";
    public static final String MENU_HELP = "Help";
    public static final String MENU_EXIT = "Exit";

    private PuzzleBoard puzzleBoard;

    private JPanel mainPanel;
    private JPanel statusPanel;
    private JPanel boardPanel;
    private JMenu menu;

    private JPopupMenu popup;

    private Color backgroundColor;

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }


    private Color borderColor = Color.RED;

    public PuzzleWindow(PuzzleBoard puzzleBoard) throws IOException {
        super(MAIN_WINDOW_TITLE);

        this.puzzleBoard = puzzleBoard;
        puzzleBoard.fillBoardAscending();

        createMenu();
        createStatusFrame();
        createBoardFrame();
        createMainFrame();
        createPopupMenu();

        refreshView();
        display();
    }


    private void createPopupMenu() throws IOException {
        popup = new JPopupMenu();
        String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();
//MS:TODO завести метод или переменную getRandomAction чтобы не было дублирования
        popup.add(new RandomAction("Random", new ImageIcon(String.format("%s/random.jpg", imgRoot)),
                KeyEvent.VK_R, puzzleBoard, this));
        popup.add(new JMenuItem("Gathered"));
        popup.add(new JMenuItem("Background color"));
        popup.add(new JMenuItem("Border color"));
        popup.setInvoker(statusPanel);
    }

    private void createMenu() throws IOException {
        JMenuBar menuBar = new JMenuBar();
        menu = new JMenu("Menu");
        String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();

//        JMenuItem menuRandom = new JMenuItem(MENU_RANDOM);
//        menuRandom.setActionCommand(MENU_RANDOM);
//        menuRandom.setMnemonic('r');
//        menuRandom.setIcon(new ImageIcon(String.format("%s/random.jpg", imgRoot)));


        JMenuItem menuGathered = new JMenuItem(MENU_GATHERED);
        menuGathered.setActionCommand(MENU_GATHERED);
        menuGathered.setMnemonic('g');
        menuGathered.setIcon(new ImageIcon(String.format("%s/gathered.jpg", imgRoot)));

        JMenuItem menuBackgroundColor = new JMenuItem(MENU_BACKGROUND_COLOR);
        menuBackgroundColor.setActionCommand(MENU_BACKGROUND_COLOR);
        menuBackgroundColor.setMnemonic('a');
        menuBackgroundColor.setIcon(new ImageIcon(String.format("%s/background.png", imgRoot)));

        JMenuItem menuBorderColor = new JMenuItem(MENU_BORDER_COLOR);
        menuBorderColor.setActionCommand(MENU_BORDER_COLOR);
        menuBorderColor.setMnemonic('o');
        menuBorderColor.setIcon(new ImageIcon(String.format("%s/border.jpg", imgRoot)));

        JMenuItem menuHelp = new JMenuItem(MENU_HELP);
        menuHelp.setActionCommand(MENU_HELP);
        menuHelp.setMnemonic('h');
        menuHelp.setIcon(new ImageIcon(String.format("%s/help.jpg", imgRoot)));

        JMenuItem menuExit = new JMenuItem(MENU_EXIT);
        menuExit.setActionCommand(MENU_EXIT);
        menuExit.setMnemonic('e');
        menuExit.setIcon(new ImageIcon(String.format("%s/exit.jpg", imgRoot)));

        //menu.add(menuRandom);

        menu.add(new RandomAction("Random", new ImageIcon(String.format("%s/random.jpg", imgRoot)),
                KeyEvent.VK_R,
                puzzleBoard, this));

        menu.add(menuGathered);
        menu.addSeparator();
        menu.add(menuBackgroundColor);
        menu.add(menuBorderColor);
        menu.addSeparator();
        menu.add(menuHelp);
        menu.add(menuExit);

        menuBar.add(menu);
        setJMenuBar(menuBar);
    }

    private void createStatusFrame() {
        statusPanel = new JPanel(new GridLayout(4, 1));
        statusPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        JLabel label1 = new JLabel("Play");
        JLabel label2 = new JLabel("Now!");

        label1.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        label1.setText("Play");
        label2.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        label2.setText("Now!");

        statusPanel.add(label1);
        statusPanel.add(label2);
    }

    private void createBoardFrame() {
        boardPanel = new JPanel(new GridLayout(puzzleBoard.getBoardSize(), puzzleBoard.getBoardSize()));
        boardPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
            for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
                final JButton boardButton = new JButton();
                boardButton.setFocusable(false);

                boardButton.setActionCommand(Integer.toString(getNumberOfComponent(y, x)));
                boardPanel.add(boardButton);
            }
        }
    }

    private void createMainFrame() {
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.red));

        mainPanel.add(statusPanel, new GridBagConstraints(
                0, 0, 1, 2, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(5, 5, 5, 5), 0, 0)
        );

        mainPanel.add(boardPanel, new GridBagConstraints(
                1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(5, 0, 5, 0), 0, 0)
        );

        mainPanel.setFocusable(true);
        getContentPane().add(mainPanel, BorderLayout.CENTER);

        setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public void refreshView() throws IOException {
        for (int i = 0; i < puzzleBoard.getBoardSize(); i++) {
            for (int j = 0; j < puzzleBoard.getBoardSize(); j++) {
                JButton boardButton = (JButton) boardPanel.getComponent(getNumberOfComponent(i, j));

                String strCellsValue = Integer.toString(puzzleBoard.getCellValue(i, j));

                if (strCellsValue.equals(Integer.toString(EMPTY_CELL))) {
                    strCellsValue = "";  // последнюю клетку рисуем пустую
                    boardButton.setBorder(BorderFactory.createLineBorder(borderColor));
                } else {
                    boardButton.setBorder(BorderFactory.createLineBorder(Color.CYAN));
                }

                try {
                    String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();
                    boardButton.setIcon(new ImageIcon(String.format("%s/ku_%s.jpg", imgRoot, strCellsValue)));
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        mainPanel.setBackground(backgroundColor);
        statusPanel.setBackground(backgroundColor);
    }

    public void display() {
        setVisible(true);
    }

    public int getNumberOfComponent(int y, int x) {
        return y * puzzleBoard.getBoardSize() + x;
    }

    public JPanel getBoardPanel() {
        return boardPanel;
    }

    public JMenu getMenu() {
        return menu;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public JPanel getStatusPanel() {
        return statusPanel;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }


    public void addMenuActionListener(MenuActionListener menuActionListener) {
        for (int i = 0; i < getMenu().getItemCount(); i++) {
            JMenuItem menuItem = getMenu().getItem(i);
            if (menuItem != null) {
                getMenu().getItem(i).addActionListener(menuActionListener);
            }
        }
    }

    public void addBoardKeyListener(KeyActionListener keyActionListener) {
        getMainPanel().addKeyListener(keyActionListener);
    }

    public void addGameFieldListener(MouseActionListener mouseActionListener) {
        for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
            for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
                JButton jButton = (JButton) getBoardPanel().getComponent(getNumberOfComponent(y, x));
                jButton.addActionListener(mouseActionListener);
            }
        }
    }

    public void displayPopupMenu(int x, int y) {
        popup.setLocation(x, y);
        popup.setVisible(true);
    }

    public JPopupMenu getPopupMenu() {
        return popup;
    }

}





