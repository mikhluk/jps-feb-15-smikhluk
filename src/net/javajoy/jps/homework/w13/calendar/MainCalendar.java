package net.javajoy.jps.homework.w13.calendar;

import java.util.Calendar;
import java.util.Locale;

/**
 * @author Sergey Mikhluk.
 */
public class MainCalendar {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance(Locale.GERMAN);

        for (int i = 31; i > 0; i--) {
            calendar.set(2015, Calendar.MAY, i);

            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                System.out.println("We found it! " + calendar.get(Calendar.YEAR) + " " + calendar.get(Calendar.MONTH)
                        + " " + calendar.get(Calendar.DAY_OF_MONTH));
                break;
            }

        }

    }

}
