package net.javajoy.jps.homework.w13.searchname;

/**
 * @author Sergey Mikhluk
 */
public class MainNameSearch {
    public static void main(String[] args) {
        String[] strPeople = new String[]{
                "Беляев",
                "Васильев",
                "Волков",
                "Волченко",
                "Воробьев",
                "Иванов",
                "Киселев",
                "Ковалев",
                "Кузьмин",
                "Кузнецов",
                "Макаров",
                "Морозов",
                "Михайлов",
                "Павлов",
                "Петров",
                "Попов",
                "Сидоров",
                "Смирнов",
        };

        new Controller(strPeople, new SearchNameForm());
    }
}
