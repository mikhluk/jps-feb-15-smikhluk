package net.javajoy.jps.homework.w13.searchname;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Sergey Mikhluk
 */
public class Controller {
    private String[] strPeople;
    private SearchNameForm form;

    public Controller(String[] strPeople, SearchNameForm form) {
        this.form = form;
        this.strPeople = strPeople;
        initListeners();
        checkName("");
    }

    private void initListeners() {
        form.getTextFieldName().getDocument().addDocumentListener(createDocumentListener());
        form.getTextFieldName().addKeyListener(createKeyAdapter());
    }

    private KeyAdapter createKeyAdapter() {
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    JTextField textField = (JTextField) e.getSource();
                    textField.setText(null);
                    form.getTextFieldName().setBackground(Color.WHITE);
                } else if ((e.getKeyCode() == KeyEvent.VK_SPACE) &&
                        ((e.getModifiers() & KeyEvent.CTRL_MASK) == KeyEvent.CTRL_MASK)) {
                    JTextField textFieldName = (JTextField) e.getSource();

                    String fullName = form.getTextPaneFoundName().getText();
                    int indexSeparator = fullName.indexOf("\n");
                    if (indexSeparator > 0) {
                        textFieldName.setText(fullName.substring(0, indexSeparator));
                    } else {
                        textFieldName.setText(fullName);
                    }
                }
            }
        };
    }

    private DocumentListener createDocumentListener() {
        return new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                getInputString(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                getInputString(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }

        };
    }

    private void getInputString(DocumentEvent e) {
        Document document = e.getDocument();
        String inputString = null;

        try {
            inputString = document.getText(0, document.getLength());
        } catch (BadLocationException e1) {
            e1.printStackTrace();
        }
        checkName(inputString);
    }

    public void checkName(String inputString) {
        String foundNames = "";

        boolean firstNameFlag = true;
        for (String name : strPeople) {
            if (name.indexOf(inputString) == 0) {
                if (firstNameFlag) {
                    foundNames = name;
                    firstNameFlag = false;
                } else {
                    foundNames += "\n" + name;
                }
            }
        }
        form.getTextPaneFoundName().setText(foundNames);

        if (foundNames.isEmpty()) {
            form.getTextFieldName().setBackground(Color.PINK);
        } else {
            form.getTextFieldName().setBackground(Color.WHITE);
        }
    }
}
