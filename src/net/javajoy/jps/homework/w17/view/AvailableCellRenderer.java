package net.javajoy.jps.homework.w17.view;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.print.Book;

/**
 * @author Sergey Mikhluk.
 */
public class AvailableCellRenderer extends JCheckBox implements TableCellRenderer {
    public AvailableCellRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setSelected((boolean)value);
        return this;
    }
}
