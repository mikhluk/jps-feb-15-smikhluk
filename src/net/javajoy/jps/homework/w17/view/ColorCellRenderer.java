package net.javajoy.jps.homework.w17.view;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @author Sergey Mikhluk.
 */
public class ColorCellRenderer extends JLabel implements TableCellRenderer {
    public ColorCellRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        int intColor;

        if ((value.getClass()).equals(String.class)) {   //MS:TODO где-то при выборе элемента цвета
            // через инструмент ColorChooser происходит смена типа на тип стринг, хотя я везде указываю int,
            // может поможете найти, чтобы там исправить?
            intColor = new Integer((String) value);
        } else {
            intColor = (int) value;
        }

        setBackground(new Color(intColor));
        if (isSelected) {
            setBorder(BorderFactory.createLineBorder(Color.RED));
        } else {
            setBorder(BorderFactory.createLineBorder(Color.WHITE));
        }
        return this;
    }
}
