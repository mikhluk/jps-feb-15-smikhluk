package net.javajoy.jps.homework.w17.view;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @author Sergey Mikhluk.
 */
public class IconCellRenderer extends JLabel implements TableCellRenderer {
    public IconCellRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            String path = (String) table.getModel().getValueAt(row, column);
            setIcon(new ImageIcon(path));

            return this;
    }
}
