package net.javajoy.jps.homework.w17.controller;

import net.javajoy.jps.homework.w17.om.Good;
import net.javajoy.jps.homework.w17.view.GoodWindow;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.file.Paths;
import java.util.Random;
import java.util.Vector;

/**
 * @author Sergey Mikhluk.
 */
public class GoodController {
    public static final int MAX_INT_COLOR = 256 * 256 * 256 - 1;
    public static final String DEFAULT_ICON_PATH = "resources/w17/defaultgood.png";

    private static GoodController instance;

    private JTable goodTableComponent;
    private DefaultTableModel goodTableModel;
    private GoodWindow goodWindow;

    private GoodController() {
    }

    public MouseListener createTableMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable table = (JTable) e.getSource();
                    if (table.getSelectedColumn() == GoodWindow.ICON_COLUMN) {
                        chooseIcon(table);
                    } else if (table.getSelectedColumn() == GoodWindow.COLOR_COLUMN) {
                        chooseColor(table);
                    }
                }
            }
        };
    }

    private void chooseColor(JTable table) {
        Color color = JColorChooser.showDialog(null, "Choose a color", null);
        int intColor = color.getRGB();
        /*
            Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException
	                     at net.javajoy.jps.homework.w17.controller.GoodController.chooseColor(GoodController.java:51)
         */

        goodTableModel.setValueAt(intColor, table.getSelectedRow(), table.getSelectedColumn());

        goodTableComponent.getColumnModel().getColumn(GoodWindow.COLOR_COLUMN).getCellEditor().
                getTableCellEditorComponent( //AN: дублирование кода
                        goodTableComponent,
                        intColor,
                        true,
                        table.getSelectedRow(),
                        table.getSelectedColumn()).
                revalidate();
        goodTableComponent.getColumnModel().getColumn(GoodWindow.COLOR_COLUMN).
                getCellEditor().stopCellEditing();

    }

    private void chooseIcon(JTable table) {
        JFileChooser fileChooser = new JFileChooser(Paths.get("./resources").toFile()); //AN: пусть вынести в константу

        FileFilter imageFilter = new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes());
        fileChooser.setFileFilter(imageFilter);
        fileChooser.setAcceptAllFileFilterUsed(false);
        int confirmation = fileChooser.showOpenDialog(null);

        if (confirmation == JFileChooser.APPROVE_OPTION) {
            goodTableModel.setValueAt(fileChooser.getSelectedFile().getPath(), table.getSelectedRow(), table.getSelectedColumn());

            goodTableComponent.getColumnModel().getColumn(GoodWindow.ICON_COLUMN).getCellEditor().
                    getTableCellEditorComponent( //AN: дублирование кода
                            goodTableComponent,
                            fileChooser.getSelectedFile().getPath(),
                            true,
                            table.getSelectedRow(),
                            table.getSelectedColumn()).
                    revalidate();
            goodTableComponent.getColumnModel().getColumn(GoodWindow.ICON_COLUMN).
                    getCellEditor().stopCellEditing();
        }
    }

    public ActionListener createAddGoodListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkRow()) {
                    addRow();
                }
            }
        };
    }

    private boolean checkRow() {
        if (goodTableModel.getRowCount() > 0) {
            int row = goodTableModel.getRowCount() - 1;

            if ((goodTableModel.getValueAt(row, GoodWindow.CATEGORY_COLUMN) == null) ||
                    (goodTableModel.getValueAt(row, GoodWindow.NAME_COLUMN) == null) ||
                    (goodTableModel.getValueAt(row, GoodWindow.PRICE_COLUMN) == null)) {

                goodWindow.getStatusLabel().setText(GoodWindow.FAILURE_TEXT + (row + 1));
                return false;
            }
        }
        goodWindow.getStatusLabel().setText(GoodWindow.OK_TEXT);
        return true;
    }

    private void addRow() {
        goodTableModel.addRow(createDefaultGood().toArray());
    }

    private Good createDefaultGood() {
        Random rnd = new Random();
        return new Good(DEFAULT_ICON_PATH, null, null, null, false, rnd.nextInt(MAX_INT_COLOR));
    }

    public ActionListener createDeleteGoodListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] selectedRows = goodTableComponent.getSelectedRows();
                for (int selectedRow : selectedRows) {
                    goodTableModel.removeRow(selectedRow);
                }
            }
        };
    }

    public ActionListener createSaveListener() { //AN: дублирование кода
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jFileChooser = createFileChooser();
                int confirmation = jFileChooser.showSaveDialog(null);
                saveOrLoad(jFileChooser, confirmation, true);
            }
        };
    }

    public ActionListener createLoadListener() { //AN: дублирование кода
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jFileChooser = createFileChooser();
                int confirmation = jFileChooser.showOpenDialog(null);
                saveOrLoad(jFileChooser, confirmation, false);
            }
        };
    }

    public static GoodController get() {
        if (instance == null)
            instance = new GoodController();
        return instance;
    }

    private void saveOrLoad(JFileChooser fileChooser, int confirmation, boolean flagSave) {
        if (confirmation == JFileChooser.APPROVE_OPTION) {
            try {
                if (flagSave) {
                    saveGameToFile(fileChooser.getSelectedFile().getName());
                } else {
                    loadGameFromFile(fileChooser.getSelectedFile().getName());
                }
            } catch (IOException | ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        }
    }

    private JFileChooser createFileChooser() {
        return new JFileChooser(Paths.get(".").toFile());
    }

    public void saveGameToFile(String fileName) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(goodTableModel.getDataVector());
        }
    }

    public void loadGameFromFile(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream ios = new ObjectInputStream(new FileInputStream(fileName));
        Object loadedItem = ios.readObject();
        //MS: добавить проверку на вектор
        goodTableModel.setDataVector((Vector)loadedItem, goodWindow.getColumnNames());
        goodWindow.initCellRendererAndEditor();
    }

    public void setGoodWindow(GoodWindow goodWindow) {
        this.goodWindow = goodWindow;
        this.goodTableComponent = goodWindow.getGoodTableComponent();
        this.goodTableModel = (DefaultTableModel) goodWindow.getGoodTableComponent().getModel();
    }
}
