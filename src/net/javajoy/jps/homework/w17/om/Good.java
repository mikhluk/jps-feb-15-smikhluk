package net.javajoy.jps.homework.w17.om;

import java.awt.*; //AN: не используется. Спроси нас как мы это замечаем

/**
 * @author Sergey Mikhluk.
 */
public class Good {
    private String icon;
    private String name;
    private Double price;
    private String category;
    private boolean availability;
    private int color;

    public Good(String icon, String name) { //AN: не используется. Спроси нас как мы это замечаем
        this.name = name;
        this.icon = icon;
    }

    public Good(String icon, String name, Double price, String category, boolean availability) {
        this.icon = icon;
        this.name = name;
        this.price = price;
        this.category = category;
        this.availability = availability;
    }

    public Good(String icon, String name, Double price, String category, boolean availability, int color) {
        this(icon, name, price, category, availability);
        this.color = color;
    }

    public Good(String icon, String name, Double price, String category, boolean availability, Object obj) { //AN: не используется. Спроси нас как мы это замечаем
        this(icon, name, price, category, availability);

    }

    public Object[] toArray() {
        Object[] array = new Object[6];
        array[0] = this.icon;
        array[1] = this.name;
        array[2] = this.price;
        array[3] = this.category;
        array[4] = this.availability;
        array[5] = this.color;
        return array;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }


}
