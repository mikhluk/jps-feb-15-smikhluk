package net.javajoy.jps.homework.w17;

import net.javajoy.jps.homework.w17.controller.GoodController;
import net.javajoy.jps.homework.w17.view.GoodWindow;

/**
 * @author Sergey Mikhluk.
 */
public class GoodMain {
    public static void main(String[] args) {
        GoodWindow goodWindow = new GoodWindow().
                useAddGoodListener(GoodController.get().createAddGoodListener()).
                useDeleteGoodListener(GoodController.get().createDeleteGoodListener()).
                useSaveListener(GoodController.get().createSaveListener()).
                useLoadListener(GoodController.get().createLoadListener()).
                useTableMouseListener(GoodController.get().createTableMouseListener());

        GoodController.get().setGoodWindow(goodWindow);
    }
}
