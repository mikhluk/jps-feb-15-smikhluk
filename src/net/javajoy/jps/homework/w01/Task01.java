package net.javajoy.jps.homework.w01;

/**
 * Массив от 3 до 35
 */
public class Task01 {

    public static void main(String[] args) {

        int[] a = new int[17];
        for (int i = 0; i < 17; i++) {
            a[i] = i * 2 + 3;
        }

        System.out.println("Выводим значение массива нечетных чисел от 3 до 35 в строку");
        for (int i = 0; i < 17; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();

        System.out.println("Выводим значение массива нечетных чисел от 3 до 35 в столбик");
        for (int i = 0; i < 17; i++) {
            System.out.println(a[i] + " ");
        }
    }
}
