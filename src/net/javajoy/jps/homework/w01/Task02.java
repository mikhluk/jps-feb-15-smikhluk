package net.javajoy.jps.homework.w01;

import java.util.Random;

/**
 * Создайте массив из 15 случайных целых чиселизотрезка[0;9].Выведите массив на экран.
 * Подсчитайте cколько  в массиве чётных элементов и выведете это количество наэкран на отдельной строке.
 */

public class Task02 {

    public static void main(String[] args) {
        Random rand = new Random();
        int[] a = new int[15];
        int summa = 0;

        for (int i = 0; i < 15; i++) {
            a[i] = rand.nextInt(10);
        }

        for (int i = 0; i < 15; i++) {
            System.out.print(a[i]);
            if (a[i] % 2 == 0) {
                System.out.print(" четное");
                summa++;
            }
            System.out.print("; ");
        }
        System.out.println();
        System.out.println("Количество четных элементов:" + summa);
    }
}