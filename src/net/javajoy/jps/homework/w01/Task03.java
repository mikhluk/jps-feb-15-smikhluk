package net.javajoy.jps.homework.w01;

import java.util.Random;

/**
 * Создайте массив из 8 случайных целых чисел из отрезка [1;10].
 * Выведите массив на экран в строку. Замените каждый элемент с нечётным индексом на ноль.
 * Снова выведете массив на экран на отдельной строке.
 */
public class Task03 {

    public static void main(String[] args) {
        Random rand = new Random();
        int[] array = new int[8];

        for (int i = 0; i < 8; i++) {
            array[i] = rand.nextInt(10) + 1;
        }

        System.out.println("Массив из 8 случайных целых чисел от 1 до 10");
        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println();
        System.out.println("Заменяем каждый элемент с нечетным индексом на 0");
        for (int i = 0; i < 8; i++) {
            if (i % 2 == 0) {
                array[i] = 0;
            }
            System.out.print(array[i] + " ");
        }
    }
}
