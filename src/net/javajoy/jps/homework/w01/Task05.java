package net.javajoy.jps.homework.w01;

// Task 05. Числа Фибоначчи
public class Task05 {
    public static void main(String[] args) {
        int[] a = new int[20];
        a[0] = 0;
        a[1] = 1;
        for(int i = 2;i<20;i++){
            a[i] = a[i-2] + a[i-1];
        }

        for(int i = 0;i<20;i++){
            System.out.print(a[i] +" ");
        }
    }
}
