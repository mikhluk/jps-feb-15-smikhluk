package net.javajoy.jps.homework.w01;

/**
 * Created by Serg on 05.03.2015.
 * Сумма чисел от 1 до 100
 */
// AS: общее: для форматирования кода - Ctrl+Alt+L

public class TaskMain {

    public static final int HUNDRED = 100;
    public static final int RANGE = 11;
    public static final int TEN = 10;

    public static void main(String[] args) {
        int sum = 0;
        int i10 = 1;

        int[] a = new int[RANGE];

        System.out.println("1. Сумма чисел от 1 до 100");
        for (int i = 1; i <= HUNDRED; i++) {
            sum = sum + i;
        }
        System.out.println(sum);

        System.out.println("2. Промежуточная сумма каждых 10 элементов");
        sum = 0;
        int j = 1;
        for (int i = 1; i <= HUNDRED; i++) {
            sum = sum + i;
            if (i10 == TEN) {
                System.out.println(i + " " + sum);
                a[j] = sum;
                j++;
                i10 = 0;
            }
            i10++;
        }

        System.out.println("3. Упорядоченный по убыванию массив");
        for (int i = 1; i <= TEN; i++) {
            System.out.println(a[RANGE - i]);
        }
    }
}
