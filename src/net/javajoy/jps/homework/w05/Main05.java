package net.javajoy.jps.homework.w05;

/**
 * Home task 05
 * 1.  Требуется  реализовать поиск строк в тексте по произвольному условию. Создайте класс,
 * хранящий массив строк (методы «добавить», «изменить», «удалить»). Также создайте вложенный
 * класс-­‐итератор, который проходит по строкам, удовлетворяющим заданному условию. Само
 * условие должно задаваться методом setCondiHon( CondiHon c ), где CondiHon – абстрактный класс
 * или интерфейс, содержащий 1 (абстрактный) метод check() – проверку условия. Для вызова метода
 * setCondiHon() используйте анонимный класс. Протестируйте решение с разными условиями поиска.
 * <p/>
 * 2. В задании 1 реализуйте обработку исключений. Генерируйте и обрабатывайте исключения
 * «неправильный аргумент», «индекс за пределами массива». Создайте также
 * свой класс исключения «условие нельзя проверить», и генерируйте его в методе check().
 *
 * @author Sergey Mikhluk
 */
public class Main05 {
    public static final int CHANGE_STRING_POSITION = 1;
    public static final int ADD_STRING_POSITION = 4;
    public static final int DELETE_STRING_POSITION = 4;
    public static final int CONDITION_LENGTH = 35;
    public static final String CONDITION_SUBSTRING = "оз";

    public static void main(String[] args) {

        String[] aOStrings = {
                "1 Однажды в студеную зимнюю пору",
                "2 Я из лесу вышел, был сильный мороз",
                "3 Гляжу поднимается медленно в гору",
                "4 Лошадка везущая хворосту воз.",
                "5 И шествуя важно, в спокойствии чинном"};


        ArrayOfStrings aOfStringsExample = new ArrayOfStrings(aOStrings);
        System.out.println("ПЕЧАТАЕМ ИСХОДНЫЙ ПРИМЕР:\n" + aOfStringsExample);
        aOfStringsExample.change("--- Это замененная строка ---", CHANGE_STRING_POSITION);
        System.out.println("ПЕЧАТАЕМ ПРИМЕР С ЗАМЕНННОЙ СТРОКОЙ:\n" + aOfStringsExample);

        aOfStringsExample.add("--- Это добавленная строка ---", ADD_STRING_POSITION);
        System.out.println("ПЕЧАТАЕМ ПРИМЕР С ДОБАВЛЕННОЙ СТРОКОЙ:\n" + aOfStringsExample);

        aOfStringsExample.delete(DELETE_STRING_POSITION);
        System.out.printf("УДАЛИЛИ СТРОКУ № %d :\n %s", DELETE_STRING_POSITION, aOfStringsExample);

        //проверка срабатывания исключения ArrayIndexOutOfBoundsException
        System.out.printf("\nПОПРОБУЕМ УДАЛИТЬ СТРОКУ № %d :\n", (DELETE_STRING_POSITION + 10));
        aOfStringsExample.delete(DELETE_STRING_POSITION + 10);
        System.out.printf("РЕЗУЛЬТАТ ПОСЛЕ ПОПЫТКИ УДАЛЕНИЯ СТРОКИ \n %s", aOfStringsExample);

        //MS: TODO не смог реализовать exception неправильный агрумент, и условие нельзя проверить, помогите пожалуйста

        // AS: методы именуются со строчной
        DemoCheckConditionLength(aOfStringsExample);
        DemoCheckConditionIncludesSubString(aOfStringsExample);
    }

    public static void DemoCheckConditionLength(ArrayOfStrings aOfStringsExample) {
        //реализовываем проверку по условию через анонимный класс
        aOfStringsExample.setCondition(new Condition() {
            @Override
            public boolean check(String string) {
                // AS: если строка нулл или пустая тогда можно выбросить исключение "условие не может быть проверено"
                return string.length() > CONDITION_LENGTH;
            }
        });

        System.out.println("\nПЕЧАТАЕМ ЧЕРЕЗ ИТЕРАТОР ВЫБОРКУ СТРОК ПО УСЛОВИЮ - ДЛИНА СТРОКИ > " + CONDITION_LENGTH);
        printThroughIterator(aOfStringsExample);
    }

    public static void DemoCheckConditionIncludesSubString(ArrayOfStrings aOfStringsExample) {
        //реализовываем проверку по условию через анонимный класс
        aOfStringsExample.setCondition(new Condition() {
            @Override
            public boolean check(String string) {
                return string.contains(CONDITION_SUBSTRING);
            }
        });

        System.out.println("\nПЕЧАТАЕМ ЧЕРЕЗ ИТЕРАТОР ВЫБОРКУ СТРОК ПО УСЛОВИЮ - СОДЕРЖИТ ПОДСТРОКУ: " + CONDITION_SUBSTRING);
        printThroughIterator(aOfStringsExample);
    }

    private static void printThroughIterator(ArrayOfStrings aOfStringsExample) {
        ArrayOfStrings.Iterator iterator = aOfStringsExample.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}
