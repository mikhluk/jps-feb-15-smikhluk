package net.javajoy.jps.homework.w05;

/**
 * @author Sergey Mikhluk
 */
public abstract class Condition {

    /**
     * Проверяет удовлетворяет ли условию элемент массива строк
     *
     * @return boolean true если элемент удовлетворяет условию
     */
    abstract boolean check(String string);
}







