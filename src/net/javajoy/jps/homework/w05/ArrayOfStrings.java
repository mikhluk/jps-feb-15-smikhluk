package net.javajoy.jps.homework.w05;

import java.util.Arrays;

/**
 * Class ArrayOfStrings implements strings processing.
 * <p/>
 *
 * @author Sergey Mikhluk
 */
public class ArrayOfStrings {

    private String[] arrayOfStrings;
    private Condition condition;

    /**
     * Constructor
     *
     * @return new instance of ArrayOfStrings
     */
    public ArrayOfStrings(String[] aOfStrings) {
        int arrayDimension = aOfStrings.length;
        this.arrayOfStrings = new String[arrayDimension];
        System.arraycopy(aOfStrings, 0, arrayOfStrings, 0, arrayDimension);
    }

    /**
     * Inner class Iterator
     */
    public class Iterator {

        private int position = 0;

        public boolean hasNext() {
            for (int i = position; i < arrayOfStrings.length; i++) {
                if (condition.check(arrayOfStrings[i])) {
                    position = i;
                    return true;
                }
            }
            return false;
        }

        /**
         * Print next element if it satisfies the condition
         */
        public String next() {
            if (position >= arrayOfStrings.length) {
                return null;
            }

            // ищем по условию с текущей позиции и до последнего элемента массива строк
            for (int i = position; i < arrayOfStrings.length; i++) {
                if (condition.check(arrayOfStrings[i])) {
                    position = i;
                    return arrayOfStrings[position++];
                }
            }
            return null;
        }
    }

    /**
     * Create iterator
     *
     * @return new instance of iterator
     */
    public Iterator iterator() {
        return new Iterator();
    }

    /**
     * Метод заменяет строку в указанной позиции в массиве строк
     *
     * @param str      новая добавляемая строка
     * @param position на какую позицию вставляется новая строка
     */
    public void change(String str, int position) {
        // TODO: AS: индекс должен быть в диапазоне от 0 до size-1
        // иначе - выбрасываем свое исключение, напр. WrongIndexException
        if (position < 0) {
            // AS: throw custom exception
        }
        arrayOfStrings[position - 1] = str;
    }

    /**
     * Метод добавляет новую строку на указанную позицию в массив строк
     * Строки идущие за указанной позицией сдвигаются на одну вперед
     *
     * @param str      новая добавляемая строка
     * @param position на какую позицию вставляется новая строка
     */
    public void add(String str, int position) {
        // AS: аналогичная проверка как в change()
        int arrayDimension = arrayOfStrings.length;
        String[] tempArrayOfStrings = new String[arrayDimension];
        System.arraycopy(arrayOfStrings, 0, tempArrayOfStrings, 0, arrayDimension);

        this.arrayOfStrings = new String[arrayDimension + 1];

        //this.arrayOfStrings[position-1] = str;
        System.arraycopy(tempArrayOfStrings, 0, arrayOfStrings, 0, position - 1);
        this.arrayOfStrings[position - 1] = str;
        System.arraycopy(tempArrayOfStrings, position - 1, arrayOfStrings, position, arrayDimension - position + 1);
    }

    /**
     * Метод удаляет строку в указанной позиции в массиве строк
     * Строки идущие за указанной позицией сдвигаются на одну назад заменяя удаленную
     *
     * @param position на какой позиции удаляется строка
     */
    public void delete(int position) {
        // TODO: AS: проверка индекса
        // сразу рекомендую проверку вынести в отдельный метод, поскольку будет повторяться во всех методах: change, add, delete
        int arrayDimension = arrayOfStrings.length;
        String[] tempArrayOfStrings = new String[arrayDimension];
        System.arraycopy(arrayOfStrings, 0, tempArrayOfStrings, 0, arrayDimension);

        try {
            arrayOfStrings[position] = ""; // пробуем очистить строку в которой будет удаление
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Предупреждение! Выход за границы массива!");
            return;
        }

        this.arrayOfStrings = new String[arrayDimension - 1];

        System.arraycopy(tempArrayOfStrings, 0, arrayOfStrings, 0, position - 1);
        System.arraycopy(tempArrayOfStrings, position, arrayOfStrings, position - 1, arrayDimension - position);
    }

    /**
     * Метод уставнавливает условие для реализации возможности поиска по условию
     *
     * @param condition instance of Condition interface
     */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        String str = "";
        for (int counter = 0; counter < arrayOfStrings.length; counter++) {
            str = str + arrayOfStrings[counter] + "\n";
        }
        return str;
    }
}
