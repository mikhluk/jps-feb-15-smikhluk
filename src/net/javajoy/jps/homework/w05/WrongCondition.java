package net.javajoy.jps.homework.w05;

/**
 * Выбрасывает исключение если условие не может быть проверено
 */
//MS: TODO не смог найти куда его применить, помогите, пожалуйста
// AS: See class Main05, line 63
public class WrongCondition extends Exception {
    // AS: лучше добавить в название слово Exception в конце, чтобы стало понятней назначение класса
    /**
     * Конструктор
     */
    public WrongCondition() {
        super("Условие не может быть проверено!");
    }
}
