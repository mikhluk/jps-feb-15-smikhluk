package net.javajoy.jps.homework.w02.temp;

import java.util.Scanner;

/**
 * Created by Serg on 16.03.2015.
 */
public class DemoScanner {

    // AS: метод нигде не используется, мертвый код нужно удалять.
    public static void demoScanner() {
        Scanner scanner = new Scanner(System.in);

        if (scanner.hasNext()) {
            String text = scanner.next();
            System.out.println("Text:" + text);
        }
    }
}
