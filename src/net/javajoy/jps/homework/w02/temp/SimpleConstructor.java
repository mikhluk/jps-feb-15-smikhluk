package net.javajoy.jps.homework.w02.temp;

/**
 * Created by Serg on 11.03.2015.
 */


class Rock {
    Rock(){
        System.out.print("Rock ");
    }
}

public class SimpleConstructor {
    public static void main(String[] args) {
        for(int i=0;i<10;i++)
            new Rock();
    }
}

    // AS: метод нигде не используется, мертвый код нужно удалять.
//    public static void demoScanner() {
//        Scanner scanner = new Scanner(System.in);
//
//        if (scanner.hasNext()) {
//            String text = scanner.next();
//            System.out.println("Text:" + text);
//        }
//    }
