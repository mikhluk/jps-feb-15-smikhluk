package net.javajoy.jps.homework.w02;

import java.util.Scanner;

public class Main {

    public static void timeScanner(byte a[], String s) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(s);
        for (int i = 0; i < 3; i++) {
            if (scanner.hasNextByte()) {
                a[i] = scanner.nextByte();
            }
        }
        if (a[0] > 23) {
            a[0] = 0;
            System.out.println("Введено некорректное число часов");
        }
        if (a[1] > 59) {
            a[1] = 0;
            System.out.println("Введено некорректное число минут");
        }
        if (a[2] > 59) {
            a[2] = 0;
            System.out.println("Введено некорректное число секунд");
        }
    }

    // AS: могу вводить значения вне диапазона, например 55 22 33,
    // а также буквы

    public static void main(String[] args) {

        Time t1 = new Time((byte) 1, (byte) 20, (byte) 30); // создаем объект t1 через конструктор с параметрами чч мм сс
        t1.printTime();

        t1.setSecond((byte) 45);
        System.out.println("изменили секунды на 45, проверим что копирующий конструктор скопирует последнеее время с 45 секундами");
        t1.printTime();
        Time t2 = new Time(t1);  // создаем новый объект через копирующий конструктор
        t2.printTime();

        Time t3 = Time.createTimeObject((byte) 2, (byte) 20, (byte) 45);  //создаем новый объект через статический метод создания объекта
        t3.printTime();

        Time.check2Times(t1, t3);  //сравниваем время на больше меньше
        System.out.println("разница во времени в секундах = " + t1.minusTime(t1, t3));

        byte[] a = new byte[3];

        timeScanner(a, "Введите время 1 в формате чч мм сс (через пробел)");
        Time t4 = Time.createTimeObject(a[0], a[1], a[2]);
        t4.printTime();

        timeScanner(a, "Введите время 2 в формате чч мм сс (через пробел)");
        Time t5 = Time.createTimeObject(a[0], a[1], a[2]);
        t5.printTime();

        Time.check2Times(t4, t5);
        System.out.println("разница во времени в секундах = " + t4.minusTime(t4, t5));
    }

}
