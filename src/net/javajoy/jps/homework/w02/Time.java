package net.javajoy.jps.homework.w02;

public class Time {

    private byte hour;
    private byte minute;
    private byte second;

    public Time() { //конструктор по умолчанию
        hour = 0;
        minute = 0;
        second = 0;
    }

    public Time(byte hh, byte mm, byte ss) {  //конструктор с параметрами
        hour = hh;
        minute = mm;
        second = ss;
    }

    public Time(Time t) {  //копирующий конструктор
        this.hour = t.hour;
        this.minute = t.minute;
        this.second = t.second;
    }

    //геттеры и сеттеры
    public byte getHour() {
        return hour;
    }

    public byte getMinute() {
        return minute;
    }

    public byte getSecond() {
        return second;
    }

    public void setHour(byte hh) {
        this.hour = hh;
    }

    public void setMinute(byte mm) {
        this.minute = mm;
    }

    public void setSecond(byte ss) {
        this.second = ss;
    }

    // Статический метод создания объекта Time
    public static Time createTimeObject(byte xx, byte yy, byte zz) {
        return new Time(xx, yy, zz);
    }

    public static int timeInSecond(Time time) {
        return time.getSecond() + (time.getMinute() * 60) + (time.getHour() * 60 * 60);
    }

    // Статический метод проверки на больше меньше
    // В качестве параметро два аргумента типа Time
    // Выводит разницу во времени в секундах
    public static void check2Times(Time time1, Time time2) {
        if (timeInSecond(time1) >= timeInSecond(time2)) {
            System.out.println("Time1 >= Time2");
        } else {
            System.out.println("Time1 < Time2 ");
        }
        // AS: что помешало сделать третий случай, когда времена равны?
        // Также хорошо, чтобы методы работали автономно, в данном идет вывод текстовой информации, что мешает его использовать
        // в другом месте, без боязни получить эти ненужные сообщения.
        // вариант: метод может возвращать целое значение, которое бы показывало результат сравнения времен
        // 0 - когда равны, -1 - когда т1<т2, и 1 - когда т1>т2
        // и потом в месте вызова метода, анализировать полученное значение и выводить соотв. сообщение.
        // В целом твой вариант тоже рабочий, но менее гибкий
    }

    public int minusTime(Time time1, Time time2) {
        return timeInSecond(time1) - timeInSecond(time2);
    }

    public void printTime() {
        System.out.println("Время: " + hour + ":" + minute + ":" + second);
    }

}





