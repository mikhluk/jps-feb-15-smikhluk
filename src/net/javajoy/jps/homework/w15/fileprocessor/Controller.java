package net.javajoy.jps.homework.w15.fileprocessor;

/**
 * @author Sergey Mikhluk
 */
public class Controller {

    private FileProcessorForm form;

    public Controller(FileProcessorForm form) {
        this.form = form;
        initListeners();
    }

    private void initListeners() {
        form.addComboBoxListener(new ComboBoxListener(form));
    }
}
