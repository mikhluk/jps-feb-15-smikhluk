package net.javajoy.jps.homework.w15.fileprocessor;

/**
 * @author Sergey Mikhluk
 */
public class FileProcessorStart {
    public static void main(String[] args) {
        new Controller(new FileProcessorForm());
    }
}
