package net.javajoy.jps.homework.w15.fileprocessor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author Sergey Mikhluk
 */
public class ComboBoxListener implements ActionListener {

    private FileProcessorForm form;
    private File fileItemFrom;
    private File fileItemTo;

    public ComboBoxListener(FileProcessorForm form) {
        this.form = form;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox comboBox = (JComboBox) e.getSource();
        String item = (String) comboBox.getSelectedItem();

        fileItemFrom = new File(form.getTextFieldFrom().getText());
        fileItemTo = new File(form.getTextFieldTo().getText());

        if (item.equals(FileProcessorForm.Operation.COPY.getName())) { //MS:TODO можно ли тут сделать проверку через
            // switch case? до того как я поменял тип операции на enum работало через switch
            copyOrMoveItem(Boolean.FALSE);

        } else if (item.equals(FileProcessorForm.Operation.MOVE.getName())) {
            copyOrMoveItem(Boolean.TRUE);

        } else if (item.equals(FileProcessorForm.Operation.DELETE.getName())) {
            if (deleteItem(fileItemFrom)) {
                form.getTextAreaStatus().setText("Files deleted successfully!");
            } else {
                form.getTextAreaStatus().setText("Deletion wasn't done! File or directory doesn't exist.");
            }

        } else if (item.equals(FileProcessorForm.Operation.SIZE.getName())) {
            form.getTextAreaStatus().setText("Total files size: " + calculateSizeItem(fileItemFrom));
        }
    }

    private void copyOrMoveItem(boolean doMoveFlag) {
        if (fileItemFrom == null || !fileItemFrom.exists()) {
            form.getTextAreaStatus().setText("Operation wasn't done! File or directory doesn't exist.");
        } else {
            try {
                Files.walkFileTree(
                        fileItemFrom.toPath(),
                        new FileCopyVisitor(fileItemFrom.toPath(), fileItemTo.toPath(), doMoveFlag));
                if (doMoveFlag) {
                    form.getTextAreaStatus().setText("Items moved successfully!");
                } else {
                    form.getTextAreaStatus().setText("Items copied successfully!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean deleteItem(File item) {
        if (item == null || !item.exists()) {
            return false;

        } else {
            if (item.isDirectory()) {
                File[] files = item.listFiles();
                if (files != null) {
                    for (File f : files) {
                        deleteItem(f);
                    }
                }
            }
            item.delete();
            return true;
        }
    }

    private long calculateSizeItem(File item) {
        if (item == null || !item.exists()) {
            form.getTextAreaStatus().setText("File or directory doesn't exist.");
            return -1;
        } else {
            long size = 0;

            if (item.isDirectory()) {
                form.getTextAreaStatus().setText("Directory: " + item);

                File[] files = item.listFiles();
                if (files != null) {
                    for (File f : files) {
                        size += calculateSizeItem(f);
                    }
                } else {
                    return -1;
                }
            } else {
                form.getTextAreaStatus().setText("File: " + item + " - " + item.length() + " bytes");
                size = item.length();
            }
            return size;
        }
    }
}
