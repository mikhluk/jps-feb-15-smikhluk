package net.javajoy.jps.homework.w15.fileprocessor;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Sergey Mikhluk.
 */
class FileCopyVisitor extends SimpleFileVisitor<Path> {

    private Path source;
    private Path destination;

    private boolean moveFlag;

    public FileCopyVisitor(Path pathSource, Path pathDestination, boolean moveFlag) {
        this.source = pathSource;
        this.destination = pathDestination;
        this.moveFlag = moveFlag;
    }

    public FileVisitResult visitFile(Path pathSource, BasicFileAttributes fileAttributes) {
        Path pathDestination = destination.resolve(source.relativize(pathSource));
        try {
            if (moveFlag) {
                Files.move(pathSource, pathDestination, StandardCopyOption.REPLACE_EXISTING);
            } else {
                Files.copy(pathSource, pathDestination, StandardCopyOption.REPLACE_EXISTING);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult preVisitDirectory(Path pathSource, BasicFileAttributes fileAttributes) {
        Path pathDestination = destination.resolve(source.relativize(pathSource));
        try {
            if (moveFlag) {
                Files.move(pathSource, pathDestination, StandardCopyOption.REPLACE_EXISTING);
            } else {
                Files.copy(pathSource, pathDestination, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return FileVisitResult.CONTINUE;
    }
}
