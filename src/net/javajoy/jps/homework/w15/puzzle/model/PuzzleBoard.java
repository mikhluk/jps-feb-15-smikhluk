package net.javajoy.jps.homework.w15.puzzle.model;

import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import java.awt.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Sergey Mikhluk
 */
public class PuzzleBoard implements Serializable {
    public static final int FLAG_COL = 1;
    public static final int FLAG_ROW = 0;

    private static PuzzleBoard instance;
    private transient PuzzleWindow puzzleWindow;

    private Color backgroundColor;
    private Color borderColor;
    private int cells[][];

    private PuzzleBoard() {
        this.borderColor = Color.RED;
    }

    private PuzzleBoard(int boardSize) {
        this();
        this.cells = new int[boardSize][boardSize];
    }

    public static PuzzleBoard getInstance() {
        if (instance == null) {
            instance = new PuzzleBoard();
        }
        return instance;
    }

    public static PuzzleBoard createOrGetInstance(int boardSize) {
        if (instance == null) {
            instance = new PuzzleBoard(boardSize);
        }
        return getInstance();
    }

    public static void setInstance(PuzzleBoard puzzleBoard) {
        if (puzzleBoard != null) {
            instance = puzzleBoard;
        }
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
        try {
            puzzleWindow.refreshView();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
        try {
            puzzleWindow.refreshView();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void fillBoardAscending() {
        int index = 0;
        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells[0].length; y++) {
                cells[x][y] = index++;
            }
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void fillBoardRandom()  {
        List<Integer> randomList = new ArrayList<>();
        for (int i = 0; i < cells.length * cells[0].length; i++) {
            randomList.add(i);
        }
        Collections.shuffle(randomList);

        int index = 0;
        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells[0].length; y++) {
                cells[x][y] = randomList.get(index++);
            }
        }
        try {
            puzzleWindow.refreshView();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setCellValue(int x, int y, int value) {
        cells[x][y] = value;
    }

    public int getCellValue(int x, int y) {
        return cells[x][y];
    }

    public int findLastCellRow() {
        return findLastCellRowCol(FLAG_ROW);
    }

    public int findLastCellCol() {
        return findLastCellRowCol(FLAG_COL);
    }

    private int findLastCellRowCol(int flagRowOrCol) {
        int col = 0;
        int row = 0;

        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells.length; y++) {
                if (cells[x][y] == PuzzleWindow.EMPTY_CELL) {
                    row = x;
                    col = y;
                }
            }
        }

        return (flagRowOrCol == FLAG_COL) ? col : row;
    }

    public void swapCells(int x1, int y1, int x2, int y2) {
        int tempCellsValue = getCellValue(x1, y1);
        setCellValue(x1, y1, getCellValue(x2, y2));
        setCellValue(x2, y2, tempCellsValue);
    }

    public int getBoardSize() {
        return cells.length;
    }

    public void setPuzzleWindow(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
    }

}
