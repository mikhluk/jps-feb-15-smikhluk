package net.javajoy.jps.homework.w15.puzzle.controller;

import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Sergey Mikhluk.
 */
public class MenuMouseAdapter extends MouseAdapter {
    private PuzzleWindow puzzleWindow;

    public MenuMouseAdapter(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            puzzleWindow.displayPopupMenu(e.getXOnScreen(), e.getYOnScreen());
        }
    }
}



