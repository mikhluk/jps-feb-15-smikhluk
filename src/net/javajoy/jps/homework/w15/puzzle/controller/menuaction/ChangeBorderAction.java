package net.javajoy.jps.homework.w15.puzzle.controller.menuaction;

import net.javajoy.jps.homework.w15.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class ChangeBorderAction extends AbstractAction {
    public ChangeBorderAction(String name, Icon icon, int mnemonic, PuzzleWindow puzzleWindow) {
        super(name, icon);
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        PuzzleBoard.getInstance().setBorderColor(JColorChooser.showDialog(null, "Choose a border color", null));
    }
}