package net.javajoy.jps.homework.w15.puzzle.controller;

import net.javajoy.jps.homework.w15.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class KeyActionListener extends KeyAdapter {
    private PuzzleWindow puzzleWindow;

    public KeyActionListener(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        PuzzleBoard puzzleBoard = PuzzleBoard.getInstance();
        int x = puzzleBoard.findLastCellRow();  //x = Row position
        int y = puzzleBoard.findLastCellCol();  //y = Column position

        if (e.getKeyCode() == KeyEvent.VK_LEFT && y > 0) {
            puzzleBoard.swapCells(x, y, x, y - 1);
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT && y < puzzleBoard.getBoardSize() - 1) {
            puzzleBoard.swapCells(x, y, x, y + 1);
        } else if (e.getKeyCode() == KeyEvent.VK_UP && x > 0) {
            puzzleBoard.swapCells(x, y, x - 1, y);
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN && x < puzzleBoard.getBoardSize() - 1) {
            puzzleBoard.swapCells(x, y, x + 1, y);
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
    }
}
