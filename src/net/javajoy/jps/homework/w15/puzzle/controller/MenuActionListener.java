package net.javajoy.jps.homework.w15.puzzle.controller;

import net.javajoy.jps.homework.w15.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w15.puzzle.view.HelpWindow;
import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Paths;

/**
 * @author Sergey Mikhluk
 */
public class MenuActionListener implements ActionListener {
    private PuzzleWindow puzzleWindow;

    public MenuActionListener(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem menuItem = (JMenuItem) e.getSource();
        try {
            switch (menuItem.getActionCommand()) {
                case PuzzleWindow.MENU_SAVE_GAME:
                    JFileChooser fileChooser = createFileChooser();
                    int returnVal = fileChooser.showSaveDialog(null);

                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        saveGameToFile(fileChooser.getSelectedFile().getName());
                    }
                    break;

                case PuzzleWindow.MENU_LOAD_GAME:
                    fileChooser = createFileChooser();
                    returnVal = fileChooser.showOpenDialog(null);

                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        loadGameFromFile(fileChooser.getSelectedFile().getName());
                    }
                    refreshView();
                    break;
                case PuzzleWindow.MENU_HELP:
                    new HelpWindow();
                    break;
                case PuzzleWindow.MENU_EXIT:
                    System.exit(0);
                    break;
            }

        } catch (IOException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }

    private JFileChooser createFileChooser() {
        return new JFileChooser(Paths.get(".").toFile());
    }

    private void refreshView() {
        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException("Refresh failed", e1);
        }
    }

    public void saveGameToFile(String fName) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fName))) {
            oos.writeObject(PuzzleBoard.getInstance());
        }
    }

    public void loadGameFromFile(String fName) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ios = new ObjectInputStream(new FileInputStream(fName))) {
            PuzzleBoard.setInstance((PuzzleBoard) ios.readObject());
            PuzzleBoard.getInstance().setPuzzleWindow(puzzleWindow);
        }
    }
}
