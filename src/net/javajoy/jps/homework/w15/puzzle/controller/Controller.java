package net.javajoy.jps.homework.w15.puzzle.controller;

import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class Controller {

    private PuzzleWindow puzzleWindow;

    public Controller(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        initControllers();
    }

    private void initControllers() {
        puzzleWindow.addBoardKeyListener(new KeyActionListener(puzzleWindow));
        puzzleWindow.addBoardMouseListener(new MouseActionListener(puzzleWindow));

        puzzleWindow.addMenuActionListener(new MenuActionListener(puzzleWindow));
        puzzleWindow.addMenuMouseAdapter(new MenuMouseAdapter(puzzleWindow));
    }
}
