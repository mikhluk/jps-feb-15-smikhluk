package net.javajoy.jps.homework.w15.puzzle.controller;

import net.javajoy.jps.homework.w15.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class MouseActionListener implements ActionListener {

    private PuzzleWindow puzzleWindow;

    public MouseActionListener(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boardButton = (JButton) e.getSource();
        String buttonIndex = boardButton.getActionCommand();

        Integer xClicked = new Integer(buttonIndex) / PuzzleBoard.getInstance().getBoardSize();
        Integer yClicked = new Integer(buttonIndex) % PuzzleBoard.getInstance().getBoardSize();

        checkSwapCells(xClicked, yClicked);
    }

    public void checkSwapCells(int x, int y) {
        PuzzleBoard puzzleBoard = PuzzleBoard.getInstance();

        if (y < puzzleBoard.getBoardSize() - 1 && puzzleBoard.getCellValue(x, y + 1) == PuzzleWindow.EMPTY_CELL) {
            puzzleBoard.swapCells(x, y, x, y + 1);
        }

        if (y > 0 && puzzleBoard.getCellValue(x, y - 1) == PuzzleWindow.EMPTY_CELL) {
            puzzleBoard.swapCells(x, y, x, y - 1);
        }

        if (x < puzzleBoard.getBoardSize() - 1 && puzzleBoard.getCellValue(x + 1, y) == PuzzleWindow.EMPTY_CELL) {
            puzzleBoard.swapCells(x + 1, y, x, y);
        }

        if (x > 0 && puzzleBoard.getCellValue(x - 1, y) == PuzzleWindow.EMPTY_CELL) {
            puzzleBoard.swapCells(x - 1, y, x, y);
        }

        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
    }
}

