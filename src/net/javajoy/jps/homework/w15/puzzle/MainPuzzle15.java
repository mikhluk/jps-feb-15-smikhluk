package net.javajoy.jps.homework.w15.puzzle;

import net.javajoy.jps.homework.w15.puzzle.controller.Controller;
import net.javajoy.jps.homework.w15.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w15.puzzle.view.PuzzleWindow;

import javax.swing.*;

/**
 * @author Sergey Mikhluk
 */
public class MainPuzzle15 {
    public static final int INITIAL_BOARD_SIZE = 4;

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        PuzzleBoard.createOrGetInstance(INITIAL_BOARD_SIZE);
        new Controller(new PuzzleWindow());
    }
}
