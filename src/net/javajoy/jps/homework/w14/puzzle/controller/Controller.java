package net.javajoy.jps.homework.w14.puzzle.controller;

import net.javajoy.jps.homework.w14.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w14.puzzle.view.PuzzleWindow;

/**
 * @author Sergey Mikhluk
 */
public class Controller {

    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;

    public Controller(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
        initControllers();
    }

    private void initControllers() {
        puzzleWindow.addBoardKeyListener(new KeyActionListener(puzzleBoard, puzzleWindow));
        puzzleWindow.addBoardMouseListener(new MouseActionListener(puzzleBoard, puzzleWindow));

        puzzleWindow.addMenuActionListener(new MenuActionListener(puzzleBoard, puzzleWindow));
        puzzleWindow.addMenuMouseAdapter(new MenuMouseAdapter(puzzleWindow));
    }
}
