package net.javajoy.jps.homework.w14.puzzle.controller;

import net.javajoy.jps.homework.w14.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w14.puzzle.view.HelpWindow;
import net.javajoy.jps.homework.w14.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * @author Sergey Mikhluk
 */
public class MenuActionListener implements ActionListener {
    private PuzzleWindow puzzleWindow;
    private PuzzleBoard puzzleBoard;
    private String filename = "puzzle.dat";

    public MenuActionListener(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem menuItem = (JMenuItem) e.getSource();

        if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_SAVE_GAME)) {
            saveGameToFile();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_LOAD_GAME)) {
            loadGameFromFile();
            refreshView();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_HELP)) {
            new HelpWindow();
        } else if (menuItem.getActionCommand().equals(PuzzleWindow.MENU_EXIT)) {
            System.exit(0);
        }
    }

    private void refreshView() {
        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
            throw new RuntimeException("Refresh failed", e1);
        }
    }

    public void saveGameToFile() {

        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(filename)); ) {

            for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
                for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
                    out.writeByte(puzzleBoard.getCellValue(x, y));
                }
            }

            out.writeInt(puzzleBoard.getBorderColor().getRed());
            out.writeInt(puzzleBoard.getBorderColor().getGreen());
            out.writeInt(puzzleBoard.getBorderColor().getBlue());

            out.writeInt(puzzleBoard.getBackgroundColor().getRed());
            out.writeInt(puzzleBoard.getBackgroundColor().getGreen());
            out.writeInt(puzzleBoard.getBackgroundColor().getBlue());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadGameFromFile() {
        int index = 0;
        byte[] byteArray = new byte[puzzleBoard.getBoardSize() * puzzleBoard.getBoardSize()];

        try (DataInputStream in = new DataInputStream(new FileInputStream(filename));){
            in.read(byteArray);

            int colorR = in.readInt();
            int colorG = in.readInt();
            int colorB = in.readInt();
            puzzleBoard.setBorderColor(new Color(colorR, colorG, colorB));

            colorR = in.readInt();
            colorG = in.readInt();
            colorB = in.readInt();
            puzzleBoard.setBackgroundColor(new Color(colorR, colorG, colorB));

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
            for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
                puzzleBoard.setCellValue(x, y, byteArray[index++]);
            }
        }
    }
}
