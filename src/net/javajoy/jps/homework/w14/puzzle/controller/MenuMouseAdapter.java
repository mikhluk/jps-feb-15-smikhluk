package net.javajoy.jps.homework.w14.puzzle.controller;

import net.javajoy.jps.homework.w14.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w14.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Sergey Mikhluk.
 */
public class MenuMouseAdapter extends MouseAdapter {
    private PuzzleWindow puzzleWindow;
//    private PuzzleBoard puzzleBoard;

//    public MenuMouseAdapter(PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
    public MenuMouseAdapter(PuzzleWindow puzzleWindow) {
        this.puzzleWindow = puzzleWindow;
//        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            puzzleWindow.displayPopupMenu(e.getXOnScreen(), e.getYOnScreen());
        }
    }
}



