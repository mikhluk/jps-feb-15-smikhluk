package net.javajoy.jps.homework.w14.puzzle.controller.menuactions;

import net.javajoy.jps.homework.w14.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w14.puzzle.view.PuzzleWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */

public class RandomAction extends AbstractAction {
    private PuzzleBoard puzzleBoard;
    private PuzzleWindow puzzleWindow;

    public RandomAction(String name, Icon icon, int mnemonic, PuzzleBoard puzzleBoard, PuzzleWindow puzzleWindow) {
        super(name, icon);
        this.puzzleBoard = puzzleBoard;
        this.puzzleWindow = puzzleWindow;
        putValue(MNEMONIC_KEY, mnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        puzzleBoard.fillBoardRandom();
        try {
            puzzleWindow.refreshView();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}