package net.javajoy.jps.homework.w14.puzzle.view;

import net.javajoy.jps.homework.w14.puzzle.controller.KeyActionListener;
import net.javajoy.jps.homework.w14.puzzle.controller.MenuActionListener;
import net.javajoy.jps.homework.w14.puzzle.controller.MenuMouseAdapter;
import net.javajoy.jps.homework.w14.puzzle.controller.MouseActionListener;
import net.javajoy.jps.homework.w14.puzzle.controller.menuactions.BackgroundColorAction;
import net.javajoy.jps.homework.w14.puzzle.controller.menuactions.BorderColorAction;
import net.javajoy.jps.homework.w14.puzzle.controller.menuactions.GatheredAction;
import net.javajoy.jps.homework.w14.puzzle.controller.menuactions.RandomAction;
import net.javajoy.jps.homework.w14.puzzle.model.PuzzleBoard;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

/**
 * @author Sergey Mikhluk
 */
public class PuzzleWindow extends JFrame {

    public static final String MAIN_WINDOW_TITLE = "Puzzle window 14";
    public static final String RESOURCE_PATH = "resources/w14";
    public static final int MAIN_WINDOW_WIDTH = 680;
    public static final int MAIN_WINDOW_HEIGHT = 580;
    public static final int EMPTY_CELL = 15;

    public static final String MENU_SAVE_GAME = "Save game";
    public static final String MENU_LOAD_GAME = "Load game";

    public static final String MENU_HELP = "Help";
    public static final String MENU_EXIT = "Exit";
    public static final String GAME_STATUS = "Play";
    public static final String GAME_INFO = "Now!";

    private PuzzleBoard puzzleBoard;

    private JPanel mainPanel;
    private JPanel statusPanel;
    private JPanel boardPanel;
    private JMenu mainMenu;
    private JMenu fileMenu;

    private JPopupMenu popup;

    public PuzzleWindow(PuzzleBoard puzzleBoard) throws IOException {
        super(MAIN_WINDOW_TITLE);

        this.puzzleBoard = puzzleBoard;
        puzzleBoard.fillBoardAscending();

        createMenu();
        createStatusFrame();
        createBoardFrame();
        createMainFrame();
        createPopupMenu();

        refreshView();
        display();
    }

    private GatheredAction createGatheredAction(String imgRoot) {
        return new GatheredAction("Gathered", new ImageIcon(String.format("%s/gathered.jpg", imgRoot)),
                KeyEvent.VK_G, puzzleBoard, this);
    }

    private RandomAction createRandomAction(String imgRoot) {
        return new RandomAction("Random", new ImageIcon(String.format("%s/random.jpg", imgRoot)),
                KeyEvent.VK_R, puzzleBoard, this);
    }

    private BorderColorAction createBorderColorAction(String imgRoot) {
        return new BorderColorAction("Border color", new ImageIcon(String.format("%s/border.jpg", imgRoot)),
                KeyEvent.VK_C, puzzleBoard, this);
    }

    private BackgroundColorAction createBackgroundColorAction(String imgRoot) {
        return new BackgroundColorAction("Background", new ImageIcon(String.format("%s/background.png", imgRoot)),
                KeyEvent.VK_B, puzzleBoard, this);
    }

    private void createPopupMenu() throws IOException {
        String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();

        popup = new JPopupMenu();

        popup.add(createGatheredAction(imgRoot));
        popup.add(createRandomAction(imgRoot));
        popup.add(createBackgroundColorAction(imgRoot));
        popup.add(createBorderColorAction(imgRoot));
        popup.setInvoker(statusPanel);
    }

    private void createMenu() throws IOException {
        JMenuBar menuBar = new JMenuBar();
        mainMenu = new JMenu("Menu");
        fileMenu = new JMenu("File");

        String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();

        mainMenu.add(createGatheredAction(imgRoot));
        mainMenu.add(createRandomAction(imgRoot));

        mainMenu.addSeparator();

        mainMenu.add(createBackgroundColorAction(imgRoot));
        mainMenu.add(createBorderColorAction(imgRoot));

        mainMenu.addSeparator();

        JMenuItem menuSaveGame = new JMenuItem(MENU_SAVE_GAME);
        menuSaveGame.setActionCommand(MENU_SAVE_GAME);
        menuSaveGame.setMnemonic(KeyEvent.VK_S);

        JMenuItem menuLoadGame = new JMenuItem(MENU_LOAD_GAME);
        menuLoadGame.setActionCommand(MENU_LOAD_GAME);
        menuLoadGame.setMnemonic(KeyEvent.VK_L);

        JMenuItem menuHelp = new JMenuItem(MENU_HELP);
        menuHelp.setActionCommand(MENU_HELP);
        menuHelp.setMnemonic('h');
        menuHelp.setIcon(new ImageIcon(String.format("%s/help.jpg", imgRoot)));

        JMenuItem menuExit = new JMenuItem(MENU_EXIT);
        menuExit.setActionCommand(MENU_EXIT);
        menuExit.setMnemonic('e');
        menuExit.setIcon(new ImageIcon(String.format("%s/exit.jpg", imgRoot)));

        mainMenu.add(menuHelp);
        mainMenu.add(menuExit);

        fileMenu.add(menuSaveGame);
        fileMenu.add(menuLoadGame);

        menuBar.add(mainMenu);
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);
    }

    private void createStatusFrame() {
        statusPanel = new JPanel(new GridLayout(4, 1));
        statusPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        JLabel labelGameStatus = new JLabel();
        JLabel labelGameInfo = new JLabel();

        labelGameStatus.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        labelGameStatus.setText(GAME_STATUS);
        labelGameInfo.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        labelGameInfo.setText(GAME_INFO);

        statusPanel.add(labelGameStatus);
        statusPanel.add(labelGameInfo);
    }

    private void createBoardFrame() {
        boardPanel = new JPanel(new GridLayout(puzzleBoard.getBoardSize(), puzzleBoard.getBoardSize()));
        boardPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
            for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
                final JButton boardButton = new JButton();
                boardButton.setFocusable(false);

                boardButton.setActionCommand(Integer.toString(getComponentNumber(x, y)));
                boardPanel.add(boardButton);
            }
        }
    }

    private void createMainFrame() {
        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.red));

        mainPanel.add(statusPanel, new GridBagConstraints(
                        0, 0, 1, 2, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(5, 5, 5, 5), 0, 0)
        );

        mainPanel.add(boardPanel, new GridBagConstraints(
                        1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(5, 0, 5, 0), 0, 0)
        );

        puzzleBoard.setBackgroundColor(mainPanel.getBackground());
        mainPanel.setFocusable(true);
        getContentPane().add(mainPanel, BorderLayout.CENTER);

        setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void refreshView() throws IOException {
        for (int i = 0; i < puzzleBoard.getBoardSize(); i++) {
            for (int j = 0; j < puzzleBoard.getBoardSize(); j++) {
                JButton boardButton = (JButton) boardPanel.getComponent(getComponentNumber(i, j));

                String strCellsValue = Integer.toString(puzzleBoard.getCellValue(i, j));

                if (strCellsValue.equals(Integer.toString(EMPTY_CELL))) {
                    strCellsValue = "";  // последнюю клетку рисуем пустую
                    boardButton.setBorder(BorderFactory.createLineBorder(puzzleBoard.getBorderColor()));
                } else {
                    boardButton.setBorder(BorderFactory.createLineBorder(Color.CYAN));
                }

                try {
                    String imgRoot = new File(RESOURCE_PATH).getCanonicalPath();
                    boardButton.setIcon(new ImageIcon(String.format("%s/ku_%s.jpg", imgRoot, strCellsValue)));
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        }
        mainPanel.setBackground(puzzleBoard.getBackgroundColor());
        statusPanel.setBackground(puzzleBoard.getBackgroundColor());
    }

    public void display() {
        setVisible(true);
    }

    public void addMenuActionListener(MenuActionListener menuActionListener) {
        addMenuItemActionListener(menuActionListener, mainMenu);
        addMenuItemActionListener(menuActionListener, fileMenu);
    }

    public void addMenuItemActionListener(MenuActionListener menuActionListener, JMenu menu) {
        for (int i = 0; i < menu.getItemCount(); i++) {
            JMenuItem menuItem = menu.getItem(i);
            if (menuItem != null) {
                menu.getItem(i).addActionListener(menuActionListener);
            }
        }
    }

    public void addBoardKeyListener(KeyActionListener keyActionListener) {
        getMainPanel().addKeyListener(keyActionListener);
    }

    public void addBoardMouseListener(MouseActionListener mouseActionListener) {
        for (int x = 0; x < puzzleBoard.getBoardSize(); x++) {
            for (int y = 0; y < puzzleBoard.getBoardSize(); y++) {
                JButton jButton = (JButton) getBoardPanel().getComponent(getComponentNumber(x, y));
                jButton.addActionListener(mouseActionListener);
            }
        }
    }

    public void addMenuMouseAdapter(MenuMouseAdapter menuMouseAdapter) {
        getStatusPanel().addMouseListener(menuMouseAdapter);
    }

    public void displayPopupMenu(int x, int y) {
        popup.setLocation(x, y);
        popup.setVisible(true);
    }

    // getters and setters
    public int getComponentNumber(int x, int y) {
        return x * puzzleBoard.getBoardSize() + y;
    }

    public JPanel getBoardPanel() {
        return boardPanel;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JPanel getStatusPanel() {
        return statusPanel;
    }
}





