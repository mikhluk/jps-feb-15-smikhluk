package net.javajoy.jps.homework.w14.puzzle.view;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;


/**
 * @author Sergey Mikhluk.
 */
public class HelpWindow extends JFrame {

    public static final String MAIN_WINDOW_TITLE = "Help";

    public static final int MAIN_WINDOW_WIDTH = 300;
    public static final int MAIN_WINDOW_HEIGHT = 200;
    public static final String HELP_TEXT = "Puzzle Game!\n" +
            "Version 1.00\n" +
            "21.05.2015\n" +
            "Author Sergey M.\n" +
            "All rights reserved!";

    public HelpWindow() {
        super(MAIN_WINDOW_TITLE);
        createMainFrame();
        display();
    }

    private void createMainFrame() {
        JPanel mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.red));
        mainPanel.add(createHelpTextPane());

        getContentPane().add(mainPanel, BorderLayout.CENTER);
        setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private JTextPane createHelpTextPane() {
        DefaultStyledDocument document = new DefaultStyledDocument();
        SimpleAttributeSet attributes = new SimpleAttributeSet();
        attributes.addAttribute(StyleConstants.Alignment, new Integer(StyleConstants.ALIGN_CENTER));

        try {
            document.insertString(0, HELP_TEXT, attributes);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        document.setParagraphAttributes(0, HELP_TEXT.length(), attributes, true);
        JTextPane helpTextPanel = new JTextPane();
        helpTextPanel.setStyledDocument(document);
        helpTextPanel.setEditable(false);

        return helpTextPanel;
    }

    public void display() {
        setVisible(true);
    }

}
