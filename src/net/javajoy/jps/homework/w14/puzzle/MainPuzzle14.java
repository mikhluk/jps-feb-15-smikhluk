package net.javajoy.jps.homework.w14.puzzle;

import net.javajoy.jps.homework.w14.puzzle.controller.Controller;
import net.javajoy.jps.homework.w14.puzzle.model.PuzzleBoard;
import net.javajoy.jps.homework.w14.puzzle.view.PuzzleWindow;

import javax.swing.*;

/**
 * @author Sergey Mikhluk
 */
public class MainPuzzle14 {
    public static final int INITIAL_BOARD_SIZE = 4;

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        PuzzleBoard puzzleBoard = new PuzzleBoard(INITIAL_BOARD_SIZE);
        new Controller(puzzleBoard, new PuzzleWindow(puzzleBoard));
    }

}
